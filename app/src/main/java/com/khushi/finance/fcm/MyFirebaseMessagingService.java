package com.khushi.finance.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.khushi.finance.R;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.utils.Helper;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private String TAG = "MyFirebaseMessagingService";
    private PendingIntent pendingIntent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "\nremoteMessage.getData()" + remoteMessage.getData());

        Map<String, String> map = remoteMessage.getData();

        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.e("Notification : ", String.format("%s:%s", key, value));
        }


        if (remoteMessage != null) {
            Log.e(TAG, "Body: " + remoteMessage.getNotification().getBody());
            Log.e(TAG, "Title: " + remoteMessage.getNotification().getTitle());
            Log.e(TAG, "Data: " + remoteMessage.getData());
        }

        if (remoteMessage.getData() != null) {
            sendNotification(remoteMessage);
        }
    }

    public void handleIntent(Intent intent){
        Helper.showLog("handleIntent");
    }

    private void sendNotification(RemoteMessage remoteMessage) {

        String title = remoteMessage.getNotification().getTitle().toLowerCase();
        String body = remoteMessage.getNotification().getBody();
        String dt = remoteMessage.getData().get("redirect");
        Helper.showLog("TITLE : "+ title);
        Helper.showLog("BODY : "+ body);
        Helper.showLog("REDIRECT : "+ dt);
        Map<String, String> data = remoteMessage.getData();

        for (Map.Entry<String, String> entry : data.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Helper.showLog("Notification : "+ String.format("%s:%s", key, value));
        }

        if (title.equalsIgnoreCase("new task assigned")) {
            Intent todo = new Intent(getApplicationContext(), MainActivity.class);
            todo.putExtra("from", "notification");
            todo.putExtra("to", "assignedtodo");
            todo.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            pendingIntent = PendingIntent.getActivity(this, 0/*Request code*/, todo, PendingIntent.FLAG_ONE_SHOT);
        } else if(title.equalsIgnoreCase("task completed")){
            /** ALSO GOES TO TO_DO BECAUSE TO_DO IS CONSIDERED AS A TASK */
            Intent todo = new Intent(getApplicationContext(), MainActivity.class);
            todo.putExtra("from", "notification");
            todo.putExtra("to", "todo");
            todo.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            pendingIntent = PendingIntent.getActivity(this, 0/*Request code*/, todo, PendingIntent.FLAG_ONE_SHOT);
        } else if(title.equalsIgnoreCase("new inquiry assigned")){
            Intent inquiry = new Intent(getApplicationContext(), MainActivity.class);
            inquiry.putExtra("from", "notification");
            inquiry.putExtra("to", "lead");
            inquiry.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            pendingIntent = PendingIntent.getActivity(this, 0/*Request code*/, inquiry, PendingIntent.FLAG_ONE_SHOT);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        Notification notification = notificationBuilder
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent)
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark))
                .setPriority(Notification.PRIORITY_MAX)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /*ID of notification*/, notification);
    }
}
