package com.khushi.finance.utils;

/**
 * Created by Ketan Ramani on 6/21/2018.
 */

public interface MultipartResponse {
    public void onMultipartSuccess(int requestCode, boolean isSuccess, String response);
    public void onMultipartError(int requestCode, boolean isError, String error);
}
