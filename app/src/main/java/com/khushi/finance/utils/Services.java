package com.khushi.finance.utils;

public class Services {
    public static final int WORKING_HOUR = 8;
    public static final String KHUSHI_WEBSITE = "http://www.khushifinancial.com/";
    public static final String NO_NETWORK = "No Internet Connection";
    public static final String PREF_NAME = "KhushiFinance";
    //public static final String BASE_URL = "http://technopus.com/ClientsDemo/khushi/Api/";
    public static final String BASE_URL = "http://www.khushifinancial.com/CRM/Api/";
    public static final String GET_SERVICE = BASE_URL + "get_service";
    public static final String ADD_INQUIRY = BASE_URL + "add_inquiry";
    public static final String LOGIN = BASE_URL + "login";
    public static final String REQUEST_LEAVE = BASE_URL + "request_leave";
    public static final String GET_LEAVE = BASE_URL + "get_leave";
    public static final String LEAVE_ACTION = BASE_URL + "leave_action";
    public static final String GET_CLIENT = BASE_URL + "get_client";
    public static final String GET_EMPLOYEE = BASE_URL + "get_employee";
    public static final String GET_CLIENT_DOCUMENT = BASE_URL + "get_client_document";
    public static final String GET_PAYMENT_DETAILS = BASE_URL + "get_client_payment_detail";
    public static final String GET_NEWS = BASE_URL + "get_news";
    public static final String GET_INQUIRY = BASE_URL + "get_inquiry";
    public static final String GET_TODO = BASE_URL + "get_to_do";
    public static final String GET_TODO_EMPLOYEE = BASE_URL + "get_to_do_employee ";
    public static final String ADD_TODO = BASE_URL + "add_to_do";
    public static final String ACTION_TODO = BASE_URL + "action_to_do";
    public static final String NOTIFICATION_SETTING = BASE_URL + "notification_setting";
    public static final String GET_MY_LEAVE = BASE_URL + "get_my_leave";
    public static final String GET_LEAD = BASE_URL + "get_lead";
    public static final String GET_ASSIGNED_TODO = BASE_URL + "get_assigned_to_do";
    public static final String GET_SALARY = BASE_URL + "get_salary";

    public static final String GET_LIVE = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=MSFT&outputsize=compact&datatype=json&interval=1min&apikey=I70WVOE6WOXBRMXU";
}
