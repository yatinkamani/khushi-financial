package com.khushi.finance.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.AsyncTask;
import android.os.Environment;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Ketan Ramani on 06/19/18.
 * ketanramani36@gmail.com
 */
public class DownloadFileAsync extends AsyncTask<String, String, String> {

    //TODO : To Use This Class for Downloading Any File Use Below Code
    //DownloadFileAsync downloadFileAsync = new DownloadFileAsync("Pass Your Context Here");
    //downloadFileAsync.execute("URL of Your File");

    //OR

    /*DownloadFileAsync async = new DownloadFileAsync("Pass Your Context Here"){
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Toast.makeText("Pass Your Context Here",s,Toast.LENGTH_SHORT).show();
        }
    };
        async.execute("URL of Your File");*/

    private Context context;
    private ProgressDialog prd;
    private String result = "";

    public DownloadFileAsync(Context context) {
        this.context = context;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (prd.isShowing()) {
            prd.dismiss();
        }
    }

    @Override
    protected String doInBackground(String... params) {
        //Log.e("Params",params[0].toString());

        String extension = MimeTypeMap.getFileExtensionFromUrl(params[0].toString());
        String fileName = URLUtil.guessFileName(params[0].toString(), null, extension);

        int count;

        try {
            URL url = new URL(params[0]);
            URLConnection connection = url.openConnection();
            connection.connect();

            //Get File Length
            int lengthOfFile = connection.getContentLength();

            //Locate Storage Location
            File file = new File(Environment.getExternalStorageDirectory() + "/Khushi Documents/");
            if (!file.isDirectory() && !file.exists()) {
                file.mkdirs();
            }

            File isFile = new File(Environment.getExternalStorageDirectory().getPath() + "/Khushi Documents/" + fileName);
            if (isFile.exists()) {
                result = "Already Saved";
            } else {
                //Download File
                InputStream input = new BufferedInputStream(url.openStream());
                //Save File
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/Khushi Documents/" + fileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    //Publish The Progress
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    output.write(data, 0, count);
                }

                //Close Connection
                output.flush();
                output.close();
                input.close();

                //https://droidyue.com/blog/2014/01/19/scan-media-files-in-android/

                //Below line is Working and it'll shows new downloaded file in device Gallery
                //context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/Khushi Documents/"+fileName+extension))));
                //Below Kitkat
                //context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"+ new File(Environment.getExternalStorageDirectory() + "/Khushi Documents/"+fileName+extension))));

                /*The below code will reflect new downloaded file in Gallery*/
                MediaScannerConnection.scanFile(context,
                        new String[]{new File(Environment.getExternalStorageDirectory().getPath() + "/Khushi Documents/" + fileName).getPath()},
                        new String[]{"*/*"}, null);

                switch (extension) {
                    case "png":
                        result = "Image Saved";
                        break;
                    case "mp4":
                        result = "Video Saved";
                        break;
                    case "mov":
                        result = "Video Saved";
                        break;
                    case "aac":
                        result = "Audio Saved";
                        break;
                    default:
                        result = "Download Complete";
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = "Download Failed ";
        }

        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        prd = new ProgressDialog(context);
        prd.setTitle("Downloading File...");
        prd.setMessage("Please, Wait a moment");
        prd.setIndeterminate(false);
        prd.setMax(100);
        prd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        prd.show();
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        //Update The Progress
        prd.setProgress(Integer.parseInt(values[0]));
    }
}