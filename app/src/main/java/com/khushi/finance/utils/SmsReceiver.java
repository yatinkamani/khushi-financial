package com.khushi.finance.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SmsReceiver extends BroadcastReceiver {

    //http://www.gadgetsaint.com/android/read-sms-messages-android/#.WyzgwVUzbIU

    private static SmsListener mListener;

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data = intent.getExtras();

        Object[] pdus = (Object[]) data.get("pdus");

        for (int i = 0; i < pdus.length; i++) {
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
            String sender = smsMessage.getDisplayOriginatingAddress();
            Helper.showLog("SENDER : "+sender);
            if (sender.endsWith("KHUSHI")) {
                String messageBody = smsMessage.getMessageBody();
                mListener.messageReceived(messageBody);
            } else {
                Helper.showLog("SmsReceiver : Other message sender");
            }
        }
    }
}

