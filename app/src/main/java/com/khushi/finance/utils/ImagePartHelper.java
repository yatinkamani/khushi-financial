package com.khushi.finance.utils;

/**
 * Created by Ketan Ramani on 6/21/2018.
 */

public class ImagePartHelper {
    String path;
    byte[] byteOfImage;
    String mimeType;

    public ImagePartHelper(String path, byte[] byteOfImage, String mimeType){
        this.path = path;
        this.byteOfImage = byteOfImage;
        this.mimeType = mimeType;
    }
}
