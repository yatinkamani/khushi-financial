package com.khushi.finance.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ketan Ramani 6/21/2018.
 */

public class MultipartApi {

    private Context context;
    private MultipartResponse resultCallback = null;
    private ProgressDialog prd;

    public MultipartApi(Context context, MultipartResponse resultCallback) {
        this.context = context;
        this.resultCallback = resultCallback;
    }

    public void execute(final int requestCode, final String url, final Map<String, String> bodyParameter, final Map<String, ImagePartHelper> imageParameter, final boolean showLoading) {

        if (showLoading)
            showLoader();

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                if (showLoading)
                    dismissLoader();

                String resultResponse = new String(response.data);
                if (resultCallback != null)
                    resultCallback.onMultipartSuccess(requestCode, true, resultResponse);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (showLoading)
                    dismissLoader();

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Please check your network connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "Please try again after some time!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Unauthorized access";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Oops. Connection Timeout!";
                }

                if (resultCallback != null)
                    resultCallback.onMultipartError(requestCode, true, message);
                volleyError.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                for (Map.Entry<String, String> entry : bodyParameter.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    params.put(key, value);
                    Log.e("Params (Body) : ", String.format("%s:%s", key, value));
                }

                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                Log.e("API URL : ", String.format("%s", url));
                if (!imageParameter.isEmpty()) {
                    for (Map.Entry<String, ImagePartHelper> entry : imageParameter.entrySet()) {
                        String key = entry.getKey();
                        ImagePartHelper helper = imageParameter.get(key);
                        params.put(key, new DataPart(helper.path, helper.byteOfImage, helper.mimeType));
                        Log.e("Params (Multi Part) : ", String.format("%s: %s, %s, %s", key, helper.path, helper.byteOfImage, helper.mimeType));
                    }
                }

                return params;
            }
        };

        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(multipartRequest);
    }

    private void showLoader() {
        prd = new ProgressDialog(context);
        prd.setMessage("Please, Wait a moment");
        prd.setTitle("Processing...");
        prd.show();
    }

    private void dismissLoader() {
        if (prd != null && prd.isShowing())
            prd.dismiss();
    }
}
