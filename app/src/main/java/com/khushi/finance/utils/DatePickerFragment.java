package com.khushi.finance.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Ketan Ramani on 3/21/2018.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private static EditText text;
    private static TextView textTv;
    private static Date startDate = null;

    public static DatePickerFragment getInstance(View view) {
        if(view instanceof EditText){
            text = (EditText) view;
        } else if(view instanceof TextView){
            textTv = (TextView) view;
        }

        return new DatePickerFragment();
    }

    public static DatePickerFragment getInstance(View view, Date date) {
        if(view instanceof EditText){
            text = (EditText) view;
        } else if(view instanceof TextView){
            textTv = (TextView) view;
        }

        startDate = date;

        return new DatePickerFragment();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        if(startDate==null){
            dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM,yyyy");
            dialog.getDatePicker().setMinDate(startDate.getTime());
        }

        //dialog.getDatePicker().setMaxDate(getLastDateOfMonth().getTime());
        return dialog;
    }

    public Date getLastDateOfMonth(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM,yyyy");
        if(text!=null){
            text.setText(format.format(calendar.getTime()));
            text.setSelection(text.getText().toString().trim().length());
        } else if(textTv!=null){
            textTv.setText(format.format(calendar.getTime()));
        }
    }

    public void showDatePickerDialog(Context context, View view) {
        DialogFragment newFragment = DatePickerFragment.getInstance(view);
        newFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), "DatePicker");
    }

    public void showDatePickerDialog(Context context, View view, Date date) {
        DialogFragment newFragment = DatePickerFragment.getInstance(view, date);
        newFragment.show(((AppCompatActivity) context).getSupportFragmentManager(), "DatePicker");
    }
}
