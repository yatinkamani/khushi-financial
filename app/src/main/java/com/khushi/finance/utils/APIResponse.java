package com.khushi.finance.utils;

/**
 * Created by Ketan Ramani on 3/19/2018.
 */

public interface APIResponse {
    public void onAPISuccess(int requestCode, boolean isSuccess, String response);
    public void onAPIError(int requestCode, boolean isError, String error);
}
