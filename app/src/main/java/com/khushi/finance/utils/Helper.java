package com.khushi.finance.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.khushi.finance.R;
import com.khushi.finance.model.MenuModel;
import com.khushi.finance.model.ServicesModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Helper {

    public static ArrayList<ServicesModel> servicesArrayList = new ArrayList<>();

    public static void showLog(String message) {
        //if (BuildConfig.DEBUG)
        Log.e("Khushi Finance", "" + message);
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show();
    }

    public static void hideSoftKeyboard(Context context, View view) {
        //view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public void setServices(ArrayList<ServicesModel> arrayList) {
        servicesArrayList = arrayList;
    }

    public String formattedDate(String dateToFormat) {
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMMM,yyyy");

        Date date = null;
        try {
            date = originalFormat.parse(dateToFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(date);
    }

    public String formattedDateTime(String dateTimeToFormat) {
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMMM,yyyy h:mm a");

        Date date = null;
        try {
            date = originalFormat.parse(dateTimeToFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(date);
    }

    public void ourWebsite(Context context) {
        try {
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Services.KHUSHI_WEBSITE));
            context.startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<MenuModel> getAdminMenu() {

        ArrayList<MenuModel> arrayList = new ArrayList<>();

        arrayList.add(new MenuModel("Home", R.drawable.ic_home_48, R.drawable.ic_home_185));
        arrayList.add(new MenuModel("Profile", R.drawable.ic_profile_48, R.drawable.ic_profile_185));
        arrayList.add(new MenuModel("Leave Management", R.drawable.ic_leave_management_48, R.drawable.ic_leave_management_185));
        arrayList.add(new MenuModel("Client Management", R.drawable.ic_client_management_48, R.drawable.ic_client_management_185));
        arrayList.add(new MenuModel("Manage Employee", R.drawable.ic_manage__employee_48, R.drawable.ic_manage__employee_185));
        arrayList.add(new MenuModel("Our Website", R.drawable.ic_our_website_48, R.drawable.ic_our_website_185));
        arrayList.add(new MenuModel("To Do", R.drawable.ic_todo_48, R.drawable.ic_todo_185));
        arrayList.add(new MenuModel("Assigned To Do", R.drawable.ic_assigned_todo_48, R.drawable.ic_assigned_todo_185));
        arrayList.add(new MenuModel("Manage News", R.drawable.ic_manage_news_48, R.drawable.ic_manage_news_185));
        arrayList.add(new MenuModel("Live", R.drawable.ic_live_48, R.drawable.ic_live_185));
        arrayList.add(new MenuModel("Setting", R.drawable.ic_setting_48, R.drawable.ic_setting_185));
        arrayList.add(new MenuModel("Contact Us", R.drawable.ic_contact_us_48, R.drawable.ic_contact_us_185));
        arrayList.add(new MenuModel("Logout", R.drawable.ic_logout_48, R.drawable.ic_logout_185));

        return arrayList;
    }

    public ArrayList<MenuModel> getSalesmanMenu() {

        ArrayList<MenuModel> arrayList = new ArrayList<>();

        arrayList.add(new MenuModel("Home", R.drawable.ic_home_48, R.drawable.ic_home_185));
        arrayList.add(new MenuModel("Profile", R.drawable.ic_profile_48, R.drawable.ic_profile_185));
        arrayList.add(new MenuModel("DCR (Daily Call Record)", R.drawable.ic_query_manager_48, R.drawable.ic_query_manager_185));
        arrayList.add(new MenuModel("Leave", R.drawable.ic_leave_48, R.drawable.ic_leave_185));
        arrayList.add(new MenuModel("My Leave", R.drawable.ic_my_leave_48, R.drawable.ic_my_leave_185));
        arrayList.add(new MenuModel("To Do", R.drawable.ic_todo_48, R.drawable.ic_todo_185));
        arrayList.add(new MenuModel("Assigned To Do", R.drawable.ic_assigned_todo_48, R.drawable.ic_assigned_todo_185));
        arrayList.add(new MenuModel("Our Website", R.drawable.ic_our_website_48, R.drawable.ic_our_website_185));
        arrayList.add(new MenuModel("Manage News", R.drawable.ic_manage_news_48, R.drawable.ic_manage_news_185));
        arrayList.add(new MenuModel("Live", R.drawable.ic_live_48, R.drawable.ic_live_185));
        arrayList.add(new MenuModel("Setting", R.drawable.ic_setting_48, R.drawable.ic_setting_185));
        arrayList.add(new MenuModel("Contact Us", R.drawable.ic_contact_us_48, R.drawable.ic_contact_us_185));
        arrayList.add(new MenuModel("Logout", R.drawable.ic_logout_48, R.drawable.ic_logout_185));

        return arrayList;
    }

    public ArrayList<MenuModel> getStaffMenu() {

        ArrayList<MenuModel> arrayList = new ArrayList<>();

        arrayList.add(new MenuModel("Home", R.drawable.ic_home_48, R.drawable.ic_home_185));
        arrayList.add(new MenuModel("Profile", R.drawable.ic_profile_48, R.drawable.ic_profile_185));
        arrayList.add(new MenuModel("Inquiry Manager", R.drawable.ic_query_manager_48, R.drawable.ic_query_manager_185));
        arrayList.add(new MenuModel("Lead Management", R.drawable.ic_lead_management_48, R.drawable.ic_lead_management_185));
        arrayList.add(new MenuModel("Leave", R.drawable.ic_leave_48, R.drawable.ic_leave_185));
        arrayList.add(new MenuModel("My Leave", R.drawable.ic_my_leave_48, R.drawable.ic_my_leave_185));
        arrayList.add(new MenuModel("To Do", R.drawable.ic_todo_48, R.drawable.ic_todo_185));
        arrayList.add(new MenuModel("Assigned To Do", R.drawable.ic_assigned_todo_48, R.drawable.ic_assigned_todo_185));
        arrayList.add(new MenuModel("Our Website", R.drawable.ic_our_website_48, R.drawable.ic_our_website_185));
        arrayList.add(new MenuModel("Manage News", R.drawable.ic_manage_news_48, R.drawable.ic_manage_news_185));
        arrayList.add(new MenuModel("Live", R.drawable.ic_live_48, R.drawable.ic_live_185));
        arrayList.add(new MenuModel("Setting", R.drawable.ic_setting_48, R.drawable.ic_setting_185));
        arrayList.add(new MenuModel("Contact Us", R.drawable.ic_contact_us_48, R.drawable.ic_contact_us_185));
        arrayList.add(new MenuModel("Logout", R.drawable.ic_logout_48, R.drawable.ic_logout_185));

        return arrayList;
    }

    public ArrayList<MenuModel> getUnregisteredClientMenu() {

        ArrayList<MenuModel> arrayList = new ArrayList<>();

        arrayList.add(new MenuModel("Home", R.drawable.ic_home_48, R.drawable.ic_home_185));
        arrayList.add(new MenuModel("Client Login", R.drawable.ic_login_48, R.drawable.ic_login_185));
        arrayList.add(new MenuModel("Employee Login", R.drawable.ic_login_48, R.drawable.ic_login_185));
        arrayList.add(new MenuModel("Inquiry Manager", R.drawable.ic_query_manager_48, R.drawable.ic_query_manager_185));
        arrayList.add(new MenuModel("Our Website", R.drawable.ic_our_website_48, R.drawable.ic_our_website_185));
        arrayList.add(new MenuModel("Live", R.drawable.ic_live_48, R.drawable.ic_live_185));
        arrayList.add(new MenuModel("Manage News", R.drawable.ic_manage_news_48, R.drawable.ic_manage_news_185));
        arrayList.add(new MenuModel("Contact Us", R.drawable.ic_contact_us_48, R.drawable.ic_contact_us_185));

        return arrayList;
    }

    public ArrayList<MenuModel> getRegisteredClientMenu() {

        ArrayList<MenuModel> arrayList = new ArrayList<>();

        arrayList.add(new MenuModel("Home", R.drawable.ic_home_48, R.drawable.ic_home_185));
        arrayList.add(new MenuModel("Profile", R.drawable.ic_profile_48, R.drawable.ic_profile_185));
        arrayList.add(new MenuModel("Inquiry Manager", R.drawable.ic_query_manager_48, R.drawable.ic_query_manager_185));
        arrayList.add(new MenuModel("My Document", R.drawable.ic_docuemnts_48, R.drawable.ic_docuemnts_185));
        arrayList.add(new MenuModel("Our Website", R.drawable.ic_our_website_48, R.drawable.ic_our_website_185));
        arrayList.add(new MenuModel("Manage News", R.drawable.ic_manage_news_48, R.drawable.ic_manage_news_185));
        arrayList.add(new MenuModel("Live", R.drawable.ic_live_48, R.drawable.ic_live_185));
        arrayList.add(new MenuModel("Setting", R.drawable.ic_setting_48, R.drawable.ic_setting_185));
        arrayList.add(new MenuModel("Contact Us", R.drawable.ic_contact_us_48, R.drawable.ic_contact_us_185));
        arrayList.add(new MenuModel("Logout", R.drawable.ic_logout_48, R.drawable.ic_logout_185));

        return arrayList;
    }
}
