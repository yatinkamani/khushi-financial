package com.khushi.finance.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.khushi.finance.R;
import com.khushi.finance.adapter.MultipleImageAdapter;
import com.khushi.finance.adapter.ServicesAdapter;
import com.khushi.finance.model.ServicesModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.AppHelper;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.ImagePartHelper;
import com.khushi.finance.utils.MultipartApi;
import com.khushi.finance.utils.MultipartResponse;
import com.khushi.finance.utils.RecyclerItemClickListener;
import com.khushi.finance.utils.Services;
import com.khushi.finance.utils.SmsListener;
import com.khushi.finance.utils.SmsReceiver;
import com.khushi.finance.utils.StringUtils;
import com.msg91.sendotp.library.SendOtpVerification;
import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class CreateInquiryActivity extends AppCompatActivity {

    private RecyclerView rvServices;
    private ServicesAdapter servicesAdapter;
    private ArrayList<ServicesModel> arrayListService;
    private TextInputEditText etName, etEmail, etPhoneNumber, etMessage;
    /*Multipe Image*/
    private int REQUEST_WES = 989;
    private List<Image> qImageList;
    private RecyclerView rvImages;
    private MultipleImageAdapter multipleImageAdapter;
    private Verification mVerification;
    private int RESULT_ADD_INQUIRY = 52;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_inquiry);

        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar);
        toolbarTitle.setText("Create Inquiry");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.hideSoftKeyboard(CreateInquiryActivity.this,v);
                onBackPressed();
            }
        });

        qImageList = new ArrayList<>();
        rvImages = (RecyclerView) findViewById(R.id.rvImages);
        rvImages.setLayoutManager(new GridLayoutManager(CreateInquiryActivity.this, 3));
        rvImages.setHasFixedSize(true);


        etName = (TextInputEditText) findViewById(R.id.etName);
        etEmail = (TextInputEditText) findViewById(R.id.etEmail);
        etPhoneNumber = (TextInputEditText) findViewById(R.id.etPhoneNumber);
        etMessage = (TextInputEditText) findViewById(R.id.etMessage);

        rvServices = (RecyclerView) findViewById(R.id.rvServices);
        rvServices.setLayoutManager(new GridLayoutManager(CreateInquiryActivity.this, 2));
        rvServices.setHasFixedSize(true);

        getServices();

        rvServices.addOnItemTouchListener(new RecyclerItemClickListener(CreateInquiryActivity.this, rvServices, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (!arrayListService.get(position).isSelected()) {
                    ServicesModel loop = arrayListService.get(position);
                    loop.setSelected(true);
                    arrayListService.set(position, loop);
                    servicesAdapter.notifyItemChanged(position);
                } else {
                    ServicesModel loop = arrayListService.get(position);
                    loop.setSelected(false);
                    arrayListService.set(position, loop);
                    servicesAdapter.notifyItemChanged(position);
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    public void attachSMSListener() {
        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                Helper.showLog("SMS" + messageText);
            }
        });
    }

    private void getServices() {

        arrayListService = new ArrayList<>();

        if (Helper.isNetworkAvailable(CreateInquiryActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("GET Services : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray result = object.getJSONArray("data");
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject innerObject = result.getJSONObject(i);

                                ServicesModel model = new ServicesModel();
                                model.setServiceId(innerObject.getString("service_id"));
                                model.setServiceName(innerObject.getString("service_name"));
                                model.setSelected(false);

                                arrayListService.add(model);
                            }

                            servicesAdapter = new ServicesAdapter(CreateInquiryActivity.this, arrayListService);
                            rvServices.setAdapter(servicesAdapter);
                        } else {
                            Helper.showToast(CreateInquiryActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(CreateInquiryActivity.this, error);
                    }
                }
            };

            API api = new API(CreateInquiryActivity.this, apiResponse);
            api.execute(1, Services.GET_SERVICE, new HashMap<String, String>(), true);
        } else {
            Helper.showToast(CreateInquiryActivity.this, Services.NO_NETWORK);
        }
    }

    public void onGenerateInquiry(final View view) {
        final String name = etName.getText().toString().trim();
        final String email = etEmail.getText().toString().toLowerCase().trim();
        final String phone = etPhoneNumber.getText().toString().trim();
        final String message = etMessage.getText().toString().trim();

        //***********************   Services    *************************
        ArrayList<String> services = new ArrayList<>();
        for (ServicesModel sm : arrayListService) {
            if (sm.isSelected())
                services.add(sm.getServiceId());
        }

        String[] servicesArray = services.toArray(new String[services.size()]);
        final String implodedService = StringUtils.join(servicesArray, ",");
        //***********************   End of Services    *************************

        if (TextUtils.isEmpty(name)) {
            Helper.showToast(CreateInquiryActivity.this, "Enter Name");
            etName.requestFocus();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Helper.showToast(CreateInquiryActivity.this, "Enter Valid Email Address");
            etEmail.requestFocus();
        } else if (TextUtils.isEmpty(phone)) {
            Helper.showToast(CreateInquiryActivity.this, "Enter Phone Number");
            etPhoneNumber.requestFocus();
        } else if (phone.length() < 10) {
            Helper.showToast(CreateInquiryActivity.this, "Phone number must be of 10 digit");
            etPhoneNumber.requestFocus();
        } else if (TextUtils.isEmpty(message)) {
            Helper.showToast(CreateInquiryActivity.this, "Enter Message");
            etMessage.requestFocus();
        } else if (implodedService.length() == 0) {
            Helper.showToast(CreateInquiryActivity.this, "Select at lease one service");
        } else {

            SharedPreferences preferences = getApplicationContext().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
            final String user_id = preferences.getString("user_id", "");

            if (user_id.length() > 0) {
                createInquiry(user_id, name, email, phone, message, implodedService);
            } else {
                // Un-Registered Client OTP

                view.setEnabled(false);

                AlertDialog.Builder builder = new AlertDialog.Builder(CreateInquiryActivity.this);
                LayoutInflater inflater = CreateInquiryActivity.this.getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.popup_mobile, null);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();

                final TextView tvLabel = (TextView) dialogView.findViewById(R.id.tvLabel);
                final EditText et_value = (EditText) dialogView.findViewById(R.id.et_value);
                final ImageView ivClose = (ImageView) dialogView.findViewById(R.id.ivClose);
                final TextView tvOk = (TextView) dialogView.findViewById(R.id.tv_ok);

                mVerification = SendOtpVerification.createSmsVerification
                        (SendOtpVerification
                                .config("+91" + phone)
                                .context(CreateInquiryActivity.this)
                                .senderId("KHUSHI")
                                .otplength("6")
                                .build(), new VerificationListener() {
                            @Override
                            public void onInitiated(String response) {
                                alertDialog.show();
                                view.setEnabled(true);
                            }

                            @Override
                            public void onInitiationFailed(Exception paramException) {
                                Helper.showLog("onInitiationFailed : "+paramException.getMessage());
                                view.setEnabled(true);
                            }

                            @Override
                            public void onVerified(String response) {
                                if(alertDialog.isShowing()){
                                    alertDialog.dismiss();
                                }
                                view.setEnabled(true);
                                createInquiry(user_id, name, email, phone, message, implodedService);
                            }

                            @Override
                            public void onVerificationFailed(Exception paramException) {
                                Helper.showLog("onVerificationFailed : "+paramException.getMessage());
                                try {
                                    JSONObject object = new JSONObject(paramException.getMessage());
                                    String message = object.getString("message");
                                    if(message.equalsIgnoreCase("otp_not_verified")){
                                        Helper.showToast(CreateInquiryActivity.this,"OTP Doesn't Match");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                view.setEnabled(true);
                            }
                        });
                mVerification.initiate();

                SmsReceiver.bindListener(new SmsListener() {
                    @Override
                    public void messageReceived(String messageText) {
                        et_value.setText(messageText.replaceAll("\\D+",""));
                        et_value.setSelection(et_value.getText().length());
                    }
                });

                tvOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String otp = et_value.getText().toString().trim();
                        if (TextUtils.isEmpty(otp)) {
                            Helper.showToast(CreateInquiryActivity.this, "Enter OTP");
                        } else {
                            Helper.hideSoftKeyboard(CreateInquiryActivity.this,v);
                            mVerification.verify(otp);
                        }
                    }
                });

                ivClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                    }
                });

                Helper.hideSoftKeyboard(CreateInquiryActivity.this,view);
            }

            /*Random random = new Random();
            int value = 100000 + random.nextInt(900000);

            String s = "Your one time password is : "+String.valueOf(value).substring(0,6)+":,.//][{}|-_+=()*&^%$#@!`~";
            Helper.showLog(""+ s.replaceAll("\\D+",""));*/

            //createInquiry(user_id, name, email, phone, message, implodedService);
        }
    }

    private void createInquiry(String user_id, String name, String email, String phone, String message, String implodedService) {

        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        String login_status = preferences.getString("login_status", "");

        if (Helper.isNetworkAvailable(CreateInquiryActivity.this)) {
            MultipartResponse multipartResponse = new MultipartResponse() {
                @Override
                public void onMultipartSuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Add Inquiry : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            Helper.showToast(CreateInquiryActivity.this, message);
                            Intent intent = new Intent();
                            intent.putExtra("inquiry","added");
                            setResult(RESULT_ADD_INQUIRY,intent);
                            finish();
                        } else {
                            Helper.showToast(CreateInquiryActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onMultipartError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(CreateInquiryActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("name", name);
            params.put("email", email);
            params.put("phone", phone);
            params.put("message", message);
            params.put("services_name", implodedService);
            params.put("login_status",login_status);
            if (user_id.length() > 0) {
                params.put("user_id", user_id);
            }

            HashMap<String, ImagePartHelper> imageParam = new HashMap<>();
            for (int i = 0; i < qImageList.size(); i++) {
                Helper.showLog("image" + (i + 1) + " :: " + qImageList.get(i).getPath());
                imageParam.put("image" + (i + 1), new ImagePartHelper(qImageList.get(i).getPath(), AppHelper.getByteArrayFromPath(qImageList.get(i).getPath()), "image/*"));
            }

            MultipartApi multipartApi = new MultipartApi(CreateInquiryActivity.this, multipartResponse);
            multipartApi.execute(1, Services.ADD_INQUIRY, params, imageParam, true);
        } else {
            Helper.showToast(CreateInquiryActivity.this, Services.NO_NETWORK);
        }
    }

    public void onAttachImage(View view) {
        onPicImage();
    }

    private void BrowseImage() {
        ImagePicker.create(this)
                .folderMode(true) // folder mode (false by default)
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Pic images") // image selection title
                .toolbarArrowColor(Color.WHITE) // Toolbar 'up' arrow color
                .multi() // multi mode (default mode)
                .limit(5) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Khushi Captured") // directory name for captured image  ("Camera" folder by default)
                .theme(R.style.MultipleImage)
                .enableLog(true) // disabling log
                .start(); // start image picker activity with request code
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {

            qImageList.clear();
            qImageList = ImagePicker.getImages(data);

            multipleImageAdapter = new MultipleImageAdapter(CreateInquiryActivity.this, qImageList);
            rvImages.setAdapter(multipleImageAdapter);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onPicImage() {
        try {
            int permissionCheck = PermissionChecker.checkCallingOrSelfPermission(CreateInquiryActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                BrowseImage();
            } else {
                ActivityCompat.requestPermissions(CreateInquiryActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WES);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WES) {
            int permissionCheck = PermissionChecker.checkCallingOrSelfPermission(CreateInquiryActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                BrowseImage();
            }
        }
    }
}
