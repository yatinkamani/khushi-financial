package com.khushi.finance.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.LeaveModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.Services;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class LeaveDetailsActivity extends AppCompatActivity {

    private TextView tv_lv_name, tv_lv_date, tv_lv_reason, tv_lv_apply_date, tv_lv_status, tv_lv_reason_to_approve_decline, tv_lv_hours;
    private LeaveModel model;
    private LinearLayout llApproveDisApprove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_details);

        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar);
        toolbarTitle.setText("Leave Details");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_lv_name = (TextView) findViewById(R.id.tv_lv_name);
        tv_lv_date = (TextView) findViewById(R.id.tv_lv_date);
        tv_lv_reason = (TextView) findViewById(R.id.tv_lv_reason);
        tv_lv_apply_date = (TextView) findViewById(R.id.tv_lv_apply_date);
        tv_lv_status = (TextView) findViewById(R.id.tv_lv_status);
        tv_lv_reason_to_approve_decline = (TextView) findViewById(R.id.tv_lv_reason_to_approve_decline);
        tv_lv_hours = (TextView) findViewById(R.id.tv_lv_hours);
        llApproveDisApprove = (LinearLayout) findViewById(R.id.llApproveDisApprove);

        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        String designation = preferences.getString("designation", "");

        if(designation.toLowerCase().equalsIgnoreCase("admin")){
            llApproveDisApprove.setVisibility(View.VISIBLE);
        }

        if (getIntent() != null) {
            model = (LeaveModel) getIntent().getSerializableExtra("leave");
            tv_lv_name.setText(model.getUserName());
            tv_lv_date.setText(model.getStartDate() + " - " + model.getEndDate());
            tv_lv_reason.setText(model.getReasonToLeave());
            tv_lv_apply_date.setText(formattedDate(model.getLeaveCreateTime()));
            tv_lv_status.setText(model.getStatus());
            tv_lv_reason_to_approve_decline.setText(model.getReasonToApprove());
            tv_lv_hours.setText(model.getHours());
        }
    }

    public void onApprovedLeave(View view) {
        showPopup("Approved");
    }

    public void onDisApprovedLeave(View view) {
        showPopup("Declined");
    }

    public String formattedDate(String dateToFormat) {
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat targetFormat = new SimpleDateFormat("dd MMMM,yyyy h:mm a");

        Date date = null;
        try {
            date = originalFormat.parse(dateToFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetFormat.format(date);
    }

    public void showPopup(final String status) {

        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        final String user_id = preferences.getString("user_id", "");

        AlertDialog.Builder builder = new AlertDialog.Builder(LeaveDetailsActivity.this);
        LayoutInflater inflater = LeaveDetailsActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.popup_reason, null);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        final EditText et_reason_to_approve = (EditText) dialogView.findViewById(R.id.et_reason_to_approve);
        final ImageView ivClose = (ImageView) dialogView.findViewById(R.id.ivClose);
        final TextView tvOk = (TextView) dialogView.findViewById(R.id.tv_ok);

        if(status.equalsIgnoreCase("Approved")){
            et_reason_to_approve.setHint("Enter reason to approve leave");
        } else if (status.equalsIgnoreCase("Declined")){
            et_reason_to_approve.setHint("Enter reason to decline leave");
        }

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reasonToAccept = et_reason_to_approve.getText().toString().trim();
                if (TextUtils.isEmpty(reasonToAccept)) {
                    Helper.showToast(LeaveDetailsActivity.this, "Enter Reason");
                } else {
                    Helper.hideSoftKeyboard(LeaveDetailsActivity.this,v);
                    if (Helper.isNetworkAvailable(LeaveDetailsActivity.this)) {
                        APIResponse apiResponse = new APIResponse() {
                            @Override
                            public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                                try {
                                    Helper.showLog("Leave Action : " + response);
                                    JSONObject object = new JSONObject(response);
                                    String status = object.getString("status");
                                    String message = object.getString("message");
                                    if (status.equalsIgnoreCase("true")) {
                                        if (alertDialog.isShowing()) {
                                            alertDialog.dismiss();
                                        }
                                        Helper.showToast(LeaveDetailsActivity.this, message);
                                        onBackPressed();
                                    } else {
                                        Helper.showToast(LeaveDetailsActivity.this, message);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onAPIError(int requestCode, boolean isError, String error) {
                                if (isError) {
                                    Helper.showToast(LeaveDetailsActivity.this, error);
                                }
                            }
                        };

                        HashMap<String, String> params = new HashMap<>();
                        params.put("user_id", user_id);
                        params.put("leave_id", model.getLeaveId());
                        params.put("reason_to_approve", reasonToAccept);
                        params.put("status", status);

                        API api = new API(LeaveDetailsActivity.this, apiResponse);
                        api.execute(1, Services.LEAVE_ACTION, params, true);
                    } else {
                        Helper.showToast(LeaveDetailsActivity.this, Services.NO_NETWORK);
                    }
                }
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
                Helper.hideSoftKeyboard(LeaveDetailsActivity.this,v);
            }
        });
    }
}
