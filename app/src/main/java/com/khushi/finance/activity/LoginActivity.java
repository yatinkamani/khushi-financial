package com.khushi.finance.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.khushi.finance.R;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.Services;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    private TextInputEditText etEmail, etPassword;
    private final int LOGIN_CODE = 9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar);
        toolbarTitle.setText("Login");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        etEmail = (TextInputEditText) findViewById(R.id.etEmail);
        etPassword = (TextInputEditText) findViewById(R.id.etPassword);
    }

    public void onLogin(View view) {

        Helper.hideSoftKeyboard(LoginActivity.this,view);

        String email = etEmail.getText().toString().toLowerCase().trim();
        String password = etPassword.getText().toString().trim();
        String token = FirebaseInstanceId.getInstance().getToken();

        Log.e("TOKEN LOGIN",""+token);
        if (TextUtils.isEmpty(email)) {
            Helper.showToast(LoginActivity.this, "Enter Email Address");
            etEmail.requestFocus();
        }/* else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Helper.showToast(LoginActivity.this, "Enter Valid Email Address");
            etEmail.requestFocus();
        }*/ else if (TextUtils.isEmpty(password)) {
            Helper.showToast(LoginActivity.this, "Enter Password");
            etPassword.requestFocus();
        } else {
            if (Helper.isNetworkAvailable(LoginActivity.this)) {
                APIResponse apiResponse = new APIResponse() {
                    @Override
                    public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                        try {
                            Helper.showLog("Login : " + response);
                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");
                            String message = object.getString("message");
                            if (status.equalsIgnoreCase("true")) {

                                JSONObject data = object.getJSONObject("data");

                                String user_id = data.getString("user_id");
                                String user_name = data.getString("user_name");
                                String first_name =data.getString("first_name");
                                String last_name = data.getString("last_name");
                                String email_id = data.getString("email_id");
                                String user_photo = data.getString("user_photo");
                                String designation_name = data.getString("designation_name");
                                String designation = data.getString("designation");
                                String login_status = data.getString("login_status");
                                String token="",notification="";

                                if(data.has("token")){
                                    token = data.getString("token");
                                }
                                if(data.has("notification")){
                                    notification = data.getString("notification");
                                }

                                SharedPreferences preferences = getApplicationContext().getSharedPreferences(Services.PREF_NAME,MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();

                                editor.putString("user_id",user_id);
                                editor.putString("user_name",user_name);
                                editor.putString("first_name",first_name);
                                editor.putString("last_name",last_name);
                                editor.putString("email_id",email_id);
                                editor.putString("user_photo",user_photo);
                                editor.putString("designation_name",designation_name);
                                editor.putString("designation",designation);
                                editor.putString("login_status",login_status);
                                editor.putString("token",token);
                                editor.putString("notification",notification);

                                editor.apply();

                                Intent intent = new Intent();
                                intent.putExtra("loginSuccess", true);
                                setResult(LOGIN_CODE, intent);
                                //setResult(RESULT_OK);
                                finish();
                            } else {
                                Helper.showToast(LoginActivity.this, message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onAPIError(int requestCode, boolean isError, String error) {
                        if (isError) {
                            Helper.showToast(LoginActivity.this, error);
                        }
                    }
                };

                HashMap<String, String> params = new HashMap<>();
                params.put("user_email", email);
                params.put("user_password", password);
                params.put("device_type", "android");
                params.put("token", token);

                API api = new API(LoginActivity.this, apiResponse);
                api.execute(1, Services.LOGIN, params, true);
            } else {
                Helper.showToast(LoginActivity.this, Services.NO_NETWORK);
            }
        }
    }
}
