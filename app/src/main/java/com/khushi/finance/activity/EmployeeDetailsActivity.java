package com.khushi.finance.activity;

import android.graphics.Color;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.khushi.finance.R;
import com.khushi.finance.model.ManageEmployeeModel;
import com.khushi.finance.view.CircleImageView;
import com.squareup.picasso.Picasso;

public class EmployeeDetailsActivity extends AppCompatActivity {

    private CollapsingToolbarLayout collapsing_toolbar;
    private CircleImageView iv_employee_img;
    private AppCompatTextView tv_user_name,tv_first_name,tv_last_name,tv_user_email,tv_personal_email,tv_user_phone,tv_user_address,tv_birthdate,tv_joining_date;
    private ManageEmployeeModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_details);

        collapsing_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsing_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapsing_toolbar.setExpandedTitleColor(Color.TRANSPARENT);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        iv_employee_img = (CircleImageView) findViewById(R.id.iv_employee_img);
        tv_user_name = (AppCompatTextView) findViewById(R.id.tv_user_name);
        tv_first_name = (AppCompatTextView) findViewById(R.id.tv_first_name);
        tv_last_name = (AppCompatTextView) findViewById(R.id.tv_last_name);
        tv_user_email = (AppCompatTextView) findViewById(R.id.tv_user_email);
        tv_personal_email = (AppCompatTextView) findViewById(R.id.tv_personal_email);
        tv_user_phone = (AppCompatTextView) findViewById(R.id.tv_user_phone);
        tv_user_address = (AppCompatTextView) findViewById(R.id.tv_user_address);
        tv_birthdate = (AppCompatTextView) findViewById(R.id.tv_birthdate);
        tv_joining_date = (AppCompatTextView) findViewById(R.id.tv_joining_date);

        if(getIntent()!=null){
            model = (ManageEmployeeModel) getIntent().getSerializableExtra("employee");

            tv_user_name.setText(model.getUserName());
            tv_first_name.setText(model.getUserFirstname());
            tv_last_name.setText(model.getUserLastname());
            tv_user_email.setText(model.getUserEmail());
            tv_personal_email.setText(model.getPersonalEmail());
            tv_user_phone.setText(model.getUserPhone());
            tv_user_address.setText(model.getUserAddress());
            tv_birthdate.setText(model.getUserBDate());
            tv_joining_date.setText(model.getUserJoiningDate());

            Picasso.get()
                    .load(model.getUserPhoto())
                    .resize(250,250)
                    .centerCrop()
                    .error(R.drawable.image_not_available)
                    .into(iv_employee_img);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
