package com.khushi.finance.activity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.utils.Services;
import com.khushi.finance.view.CircleImageView;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends AppCompatActivity {

    private CollapsingToolbarLayout collapsing_toolbar;
    private CircleImageView iv_user_image;
    private TextView tv_user_name, tv_first_name, tv_last_name, tv_designation, tv_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        collapsing_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsing_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapsing_toolbar.setExpandedTitleColor(Color.TRANSPARENT);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initView();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        String user_name = preferences.getString("user_name", "");
        String first_name = preferences.getString("first_name", "");
        String last_name = preferences.getString("last_name", "");
        String designation_name = preferences.getString("designation_name", "");
        String designation = preferences.getString("designation", "");
        String email_id = preferences.getString("email_id", "");
        String user_photo = preferences.getString("user_photo", "");

        Picasso.get()
                .load(user_photo)
                .resize(300,300)
                .centerCrop()
                .error(R.drawable.image_not_available)
                .into(iv_user_image);

        tv_user_name.setText(user_name);
        tv_first_name.setText(first_name);
        tv_last_name.setText(last_name);
        tv_designation.setText(designation_name);
        tv_email.setText(email_id);
    }

    private void initView() {
        iv_user_image = (CircleImageView) findViewById(R.id.iv_user_image);
        tv_user_name = (TextView) findViewById(R.id.tv_user_name);
        tv_first_name = (TextView) findViewById(R.id.tv_first_name);
        tv_last_name = (TextView) findViewById(R.id.tv_last_name);
        tv_designation = (TextView) findViewById(R.id.tv_designation);
        tv_email = (TextView) findViewById(R.id.tv_email);
    }
}
