package com.khushi.finance.activity;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.khushi.finance.R;
import com.khushi.finance.utils.DownloadFileAsync;
import com.squareup.picasso.Picasso;

public class ImageDetailsActivity extends AppCompatActivity {

    private ImageView iv_document_preview;
    private String image;
    private int REQUEST_WES = 989;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_details);

        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar);
        toolbarTitle.setText("Image");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        iv_document_preview = (ImageView) findViewById(R.id.iv_document_preview);

        if (getIntent() != null) {
            image = getIntent().getStringExtra("image");

            Picasso.get()
                    .load(image)
                    .fit()
                    .error(R.drawable.image_not_available)
                    .into(iv_document_preview);
        }
    }

    public void onSaveImage(View view) {
        try {
            int permissionCheck = PermissionChecker.checkCallingOrSelfPermission(ImageDetailsActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                DownloadImage();
            } else {
                ActivityCompat.requestPermissions(ImageDetailsActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WES);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void DownloadImage() {
        DownloadFileAsync async = new DownloadFileAsync(ImageDetailsActivity.this){
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Toast.makeText(ImageDetailsActivity.this,s,Toast.LENGTH_SHORT).show();
            }
        };
        async.execute(image);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WES) {
            int permissionCheck = PermissionChecker.checkCallingOrSelfPermission(ImageDetailsActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                DownloadImage();
            }
        }
    }
}
