package com.khushi.finance.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.ToDoModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.Services;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ToDoDetails extends AppCompatActivity {

    private TextView tv_td_name, tv_td_message, tv_td_status, tv_td_to;
    private ToDoModel model;
    private LinearLayout llToDoStatus;
    private int RESULT_ASSIGNED_TODO = 94;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_details);

        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar);
        toolbarTitle.setText("To Do Details");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_td_name = (TextView) findViewById(R.id.tv_td_name);
        tv_td_message = (TextView) findViewById(R.id.tv_td_message);
        tv_td_status = (TextView) findViewById(R.id.tv_td_status);
        tv_td_to = (TextView) findViewById(R.id.tv_td_to);

        llToDoStatus = (LinearLayout) findViewById(R.id.llToDoStatus);

        if (getIntent() != null) {
            model = (ToDoModel) getIntent().getSerializableExtra("todo");

            tv_td_name.setText(model.getEmployeeUsername());
            tv_td_message.setText(model.getTodoMessage());

            boolean show = getIntent().getBooleanExtra("showdonebutton", false);

            if (show && model.getTodoStatus().equalsIgnoreCase("0")) {
                llToDoStatus.setVisibility(View.VISIBLE);
            } else {
                llToDoStatus.setVisibility(View.GONE);
            }

            tv_td_to.setText(model.getTodoTo());

            String status = model.getTodoStatus();

            switch (status) {
                case "0":
                    tv_td_status.setText("In-Complete");
                    break;

                case "1":
                    tv_td_status.setText("Completed");
                    llToDoStatus.setVisibility(View.GONE);
                    break;
            }
        }
    }

    public void onToDoCompleted(View view) {
        if (Helper.isNetworkAvailable(ToDoDetails.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Action ToDo : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            Intent intent = new Intent();
                            intent.putExtra("todo","completed");
                            setResult(RESULT_ASSIGNED_TODO,intent);
                            finish();
                        } else {
                            Helper.showToast(ToDoDetails.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(ToDoDetails.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("todo_id", model.getTodoId());

            API api = new API(ToDoDetails.this, apiResponse);
            api.execute(1, Services.ACTION_TODO, params, true);
        } else {
            Helper.showToast(ToDoDetails.this, Services.NO_NETWORK);
        }
    }
}
