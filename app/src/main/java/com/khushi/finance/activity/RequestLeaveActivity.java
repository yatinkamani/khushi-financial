package com.khushi.finance.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.adapter.LeaveHourAdapter;
import com.khushi.finance.model.LeaveHourModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.DatePickerFragment;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.Services;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class RequestLeaveActivity extends AppCompatActivity implements View.OnClickListener {

    private TextInputEditText etReason;
    private TextView tvStartDate, tvEndDate;
    private AppCompatSpinner spnLeave,spnLeaveHour;
    private ArrayList<LeaveHourModel> arrayList,arrayList1;
    private int RESULT_REQUEST_LEAVE = 53;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_leave);

        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar);
        toolbarTitle.setText("Request Leave");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.hideSoftKeyboard(RequestLeaveActivity.this,v);
                onBackPressed();
            }
        });

        tvStartDate = (TextView) findViewById(R.id.tvStartDate);
        tvEndDate = (TextView) findViewById(R.id.tvEndDate);

        tvStartDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tvEndDate.setText("");
            }
        });

        etReason = (TextInputEditText) findViewById(R.id.etReason);

        arrayList = new ArrayList<>();
        arrayList.add(new LeaveHourModel("Select Leave"));
        arrayList.add(new LeaveHourModel("Full Day"));
        arrayList.add(new LeaveHourModel("Half Day"));

        spnLeave = (AppCompatSpinner) findViewById(R.id.spnLeave);
        spnLeave.setAdapter(new LeaveHourAdapter(RequestLeaveActivity.this,arrayList));
        spnLeave.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(arrayList.get(position).getLeave().equalsIgnoreCase("Half Day")){
                    spnLeaveHour.setVisibility(View.VISIBLE);
                } else {
                    spnLeaveHour.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        arrayList1 = new ArrayList<>();
        arrayList1.add(new LeaveHourModel("Select Hour"));
        arrayList1.add(new LeaveHourModel("1"));
        arrayList1.add(new LeaveHourModel("2"));
        arrayList1.add(new LeaveHourModel("3"));
        arrayList1.add(new LeaveHourModel("4"));
        arrayList1.add(new LeaveHourModel("5"));
        arrayList1.add(new LeaveHourModel("6"));
        arrayList1.add(new LeaveHourModel("7"));

        spnLeaveHour = (AppCompatSpinner) findViewById(R.id.spnLeaveHour);
        spnLeaveHour.setAdapter(new LeaveHourAdapter(RequestLeaveActivity.this,arrayList1));

        tvStartDate.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);

        tvEndDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(tvStartDate.getText().toString().trim().equalsIgnoreCase(tvEndDate.getText().toString().trim())){
                    spnLeave.setVisibility(View.VISIBLE);
                    spnLeave.setAdapter(new LeaveHourAdapter(RequestLeaveActivity.this,arrayList));
                } else {
                    spnLeave.setVisibility(View.GONE);
                    spnLeaveHour.setVisibility(View.GONE);
                }
            }
        });
    }

    public void onLeave(View view) {

        Helper.hideSoftKeyboard(RequestLeaveActivity.this,view);

        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        String user_id = preferences.getString("user_id", "");

        String startDate = tvStartDate.getText().toString().trim();
        String endDate = tvEndDate.getText().toString().trim();
        String reason = etReason.getText().toString().trim();

        if (TextUtils.isEmpty(startDate)) {
            Helper.showToast(RequestLeaveActivity.this, "Select Start Date");
        } else if (TextUtils.isEmpty(endDate)) {
            Helper.showToast(RequestLeaveActivity.this, "Select End Date");
        } else if (TextUtils.isEmpty(reason)) {
            Helper.showToast(RequestLeaveActivity.this, "Enter reason for leave");
        } else {
            if(tvStartDate.getText().toString().trim().equalsIgnoreCase(tvEndDate.getText().toString().trim())){
                if(spnLeaveHour.getVisibility()==View.VISIBLE && arrayList1.get(spnLeaveHour.getSelectedItemPosition()).getLeave().equalsIgnoreCase("Select Hour")){
                    Helper.showToast(RequestLeaveActivity.this, "Select Hour of Leave");
                } else if (spnLeave.getVisibility()==View.VISIBLE && arrayList.get(spnLeave.getSelectedItemPosition()).getLeave().equalsIgnoreCase("Select Leave")){
                    Helper.showToast(RequestLeaveActivity.this, "Select Leave");
                } else {
                    if(spnLeave.getVisibility()==View.VISIBLE && arrayList.get(spnLeave.getSelectedItemPosition()).getLeave().equalsIgnoreCase("Full Day")){
                        requestLeave(user_id, startDate, endDate, reason, String.valueOf(Services.WORKING_HOUR));
                    } else {
                        requestLeave(user_id, startDate, endDate, reason, arrayList1.get(spnLeaveHour.getSelectedItemPosition()).getLeave());
                    }
                }
            } else {
                SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
                ArrayList<Date> dt = getDates(targetFormat.format(new Date(startDate)),targetFormat.format(new Date(endDate)));
                requestLeave(user_id, startDate, endDate, reason, String.valueOf(dt.size() * Services.WORKING_HOUR));
            }
        }
    }

    private void requestLeave(String user_id, String startDate, String endDate, String reason, String hour) {
        if (Helper.isNetworkAvailable(RequestLeaveActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Request Leave : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            Helper.showToast(RequestLeaveActivity.this, message);
                            Intent intent = new Intent();
                            intent.putExtra("leave","requested");
                            setResult(RESULT_REQUEST_LEAVE,intent);
                            finish();
                        } else {
                            Helper.showToast(RequestLeaveActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(RequestLeaveActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_id);
            params.put("start_date", startDate);
            params.put("end_date", endDate);
            params.put("reason_to_leave", reason);
            params.put("hour", hour);

            API api = new API(RequestLeaveActivity.this, apiResponse);
            api.execute(1, Services.REQUEST_LEAVE, params, true);
        } else {
            Helper.showToast(RequestLeaveActivity.this, Services.NO_NETWORK);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvStartDate:
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM,yyyy");
                Date date = new Date();
                String todayDate = simpleDateFormat.format(date);

                DatePickerFragment fragmentStartDate = new DatePickerFragment();
                fragmentStartDate.showDatePickerDialog(RequestLeaveActivity.this, v, new Date(todayDate));
                break;

            case R.id.tvEndDate:
                if(tvStartDate.getText().toString().trim().length()>0){
                    DatePickerFragment fragmentEndDate = new DatePickerFragment();
                    fragmentEndDate.showDatePickerDialog(RequestLeaveActivity.this, v, new Date(tvStartDate.getText().toString().trim()));
                } else {
                    Helper.showToast(RequestLeaveActivity.this,"Please, Select start date first");
                }
                break;
        }
    }

    private ArrayList<Date> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }

        //EEE MMM dd HH:mm:ss zzz yyyy
        /*Fri Jun 01 00:00:00 GMT+05:30 2018
        Sat Jun 02 00:00:00 GMT+05:30 2018*/

        return dates;
    }
}
