package com.khushi.finance.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.NewsModel;

public class NewsDetailsActivity extends AppCompatActivity {

    private TextView tv_news_title, tv_news_date, tv_news_description;
    private NewsModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar);
        toolbarTitle.setText("News Details");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_news_title = (TextView) findViewById(R.id.tv_news_title);
        tv_news_date = (TextView) findViewById(R.id.tv_news_date);
        tv_news_description = (TextView) findViewById(R.id.tv_news_description);

        if (getIntent() != null) {
            model = (NewsModel) getIntent().getSerializableExtra("news");

            tv_news_title.setText(model.getNewsTitle());
            tv_news_date.setText(model.getNewsDate());
            tv_news_description.setText(model.getNewsDescription());
        }
    }
}
