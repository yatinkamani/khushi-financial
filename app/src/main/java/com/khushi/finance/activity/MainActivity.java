package com.khushi.finance.activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.adapter.MenuAdapter;
import com.khushi.finance.fragment.ClientManagementFragment;
import com.khushi.finance.fragment.ContactUsFragment;
import com.khushi.finance.fragment.HomeFragment;
import com.khushi.finance.fragment.InquiryFragment;
import com.khushi.finance.fragment.LeadFragment;
import com.khushi.finance.fragment.LeaveFragment;
import com.khushi.finance.fragment.LiveFragment;
import com.khushi.finance.fragment.ManageEmployeeFragment;
import com.khushi.finance.fragment.MyDocumentFragment;
import com.khushi.finance.fragment.MyLeaveFragment;
import com.khushi.finance.fragment.NewsFragment;
import com.khushi.finance.fragment.SettingFragment;
import com.khushi.finance.fragment.ToDoAssignedToMeFragment;
import com.khushi.finance.fragment.ToDoFragment;
import com.khushi.finance.model.MenuModel;
import com.khushi.finance.utils.DividerItemDecoration;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.RecyclerItemClickListener;
import com.khushi.finance.utils.Services;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private final int LOGIN_CODE = 9;
    private RecyclerView rvMenu;
    private MenuAdapter menuAdapter;
    private ArrayList<MenuModel> arrayListMenu;
    private CollapsingToolbarLayout collapsing_toolbar;
    private RelativeLayout relCollapse;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private TextView drawerName;
    private SharedPreferences preferences;
    private int REQUEST_SMS = 555;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = getApplicationContext().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);

        collapsing_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        relCollapse = (RelativeLayout) findViewById(R.id.relCollapse);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        rvMenu = (RecyclerView) findViewById(R.id.rvMenu);
        rvMenu.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        rvMenu.setHasFixedSize(true);
        rvMenu.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(MainActivity.this, R.drawable.rv_divider)));

        Fragment fragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();

        if (getIntent() != null && getIntent().getStringExtra("from") != null) {
            if (getIntent().getStringExtra("to").equalsIgnoreCase("todo")) {
                changeFragment(new ToDoFragment());
            }
            if(getIntent().getStringExtra("to").equalsIgnoreCase("assignedtodo")){
                changeFragment(new ToDoAssignedToMeFragment());
            }
            if (getIntent().getStringExtra("to").equalsIgnoreCase("lead")) {
                changeFragment(new LeadFragment());
            }
        }

        collapsing_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapsing_toolbar.setExpandedTitleColor(Color.TRANSPARENT);

        drawerName = (TextView) findViewById(R.id.drawerName);

        checkPermission();

        rvMenu.addOnItemTouchListener(new RecyclerItemClickListener(MainActivity.this, rvMenu, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (arrayListMenu.get(position).getMenuTitle()) {
                    case "Home":
                        closeDrawer();
                        changeFragment(new HomeFragment());
                        break;

                    case "Contact Us":
                        closeDrawer();
                        changeFragment(new ContactUsFragment());
                        break;

                    case "Setting":
                        closeDrawer();
                        changeFragment(new SettingFragment());
                        break;

                    case "Inquiry Manager":
                        closeDrawer();
                        String email_id = preferences.getString("email_id", "");
                        if (email_id.length() > 0) {
                            changeFragment(new InquiryFragment());
                        } else {
                            startActivity(new Intent(MainActivity.this, CreateInquiryActivity.class));
                        }
                        break;

                    case "Client Login":
                        closeDrawer();
                        startActivityForResult(new Intent(MainActivity.this, LoginActivity.class), LOGIN_CODE);
                        break;

                    case "Employee Login":
                        closeDrawer();
                        startActivityForResult(new Intent(MainActivity.this, LoginActivity.class), LOGIN_CODE);
                        break;

                    case "Logout":
                        closeDrawer();
                        clearPreferences();
                        break;

                    case "Leave":
                        closeDrawer();
                        changeFragment(new LeaveFragment());
                        break;

                    case "Our Website":
                        closeDrawer();
                        new Helper().ourWebsite(MainActivity.this);
                        break;

                    case "Leave Management": // For Admin
                        closeDrawer();
                        changeFragment(new LeaveFragment());
                        break;

                    case "Client Management":
                        closeDrawer();
                        changeFragment(new ClientManagementFragment());
                        break;

                    case "Manage Employee":
                        closeDrawer();
                        changeFragment(new ManageEmployeeFragment());
                        break;

                    case "Manage News":
                        closeDrawer();
                        changeFragment(new NewsFragment());
                        break;

                    case "Profile":
                        closeDrawer();
                        startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                        break;

                    case "DCR (Daily Call Record)":
                        closeDrawer();
                        String email_id1 = preferences.getString("email_id", "");
                        if (email_id1.length() > 0) {
                            changeFragment(new InquiryFragment());
                        } else {
                            startActivity(new Intent(MainActivity.this, CreateInquiryActivity.class));
                        }
                        break;

                    case "To Do":
                        closeDrawer();
                        changeFragment(new ToDoFragment());
                        break;

                    case "My Document":
                        closeDrawer();
                        changeFragment(new MyDocumentFragment());
                        break;

                    case "My Leave":
                        closeDrawer();
                        changeFragment(new MyLeaveFragment());
                        break;

                    case "Lead Management":
                        closeDrawer();
                        changeFragment(new LeadFragment());
                        break;

                    case "Live":
                        closeDrawer();
                        changeFragment(new LiveFragment());
                        break;

                    case "Assigned To Do":
                        closeDrawer();
                        changeFragment(new ToDoAssignedToMeFragment());
                        break;
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    private void clearPreferences() {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        updateDrawerMenu();

        FragmentManager fm = getSupportFragmentManager();
        if (fm.findFragmentById(R.id.container) instanceof HomeFragment) {
            HomeFragment fragment = (HomeFragment) fm.findFragmentById(R.id.container);
            fragment.updateDrawerMenu();

            // this will clear the back stack and displays no animation on the screen
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } else {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            changeFragment(new HomeFragment());
        }
        //Fragment f = getActivity().getFragmentManager().findFragmentById(R.id.container);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateDrawerMenu();
    }

    private void checkPermission() {
        int permissionCheck = PermissionChecker.checkCallingOrSelfPermission(MainActivity.this, Manifest.permission.READ_SMS);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.READ_SMS}, REQUEST_SMS);
        }
    }

    public void updateDrawerMenu() {
        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        String designation = preferences.getString("designation", "");
        String email_id = preferences.getString("email_id", "");
        if (email_id.length() > 0) {
            drawerName.setVisibility(View.VISIBLE);
            drawerName.setText("Hello, " + preferences.getString("first_name", "") + " " + preferences.getString("last_name", ""));
        } else {
            drawerName.setVisibility(View.GONE);
        }

        switch (designation) {

            case "admin":
                arrayListMenu = new Helper().getAdminMenu();
                menuAdapter = new MenuAdapter(MainActivity.this, arrayListMenu, 1);
                rvMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;

            case "Salesman":
                arrayListMenu = new Helper().getSalesmanMenu();
                menuAdapter = new MenuAdapter(MainActivity.this, arrayListMenu, 1);
                rvMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;

            case "Staff":
                arrayListMenu = new Helper().getStaffMenu();
                menuAdapter = new MenuAdapter(MainActivity.this, arrayListMenu, 1);
                rvMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;

            case "client":
                arrayListMenu = new Helper().getRegisteredClientMenu();
                menuAdapter = new MenuAdapter(MainActivity.this, arrayListMenu, 1);
                rvMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;

            case "super_admin":
                arrayListMenu = new Helper().getAdminMenu();
                menuAdapter = new MenuAdapter(MainActivity.this, arrayListMenu, 1);
                rvMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;

            default:
                arrayListMenu = new Helper().getUnregisteredClientMenu();
                menuAdapter = new MenuAdapter(MainActivity.this, arrayListMenu, 1);
                rvMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;
        }
    }

    public void closeDrawer() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void changeFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void toolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void disableCollapse() {
        this.relCollapse.setVisibility(View.GONE);
        this.collapsing_toolbar.setTitleEnabled(false);
    }

    public void enableCollapse() {
        this.relCollapse.setVisibility(View.VISIBLE);
        this.collapsing_toolbar.setTitleEnabled(true);
    }

    public void updateCollpaseData(String imagePath, String name) {
        this.collapsing_toolbar.setTitle(getString(R.string.title_activity_main));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_CODE && resultCode == LOGIN_CODE) {
            Log.e("ADMIN","ADMIN");
            getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            changeFragment(new HomeFragment());
        }
    }
}
