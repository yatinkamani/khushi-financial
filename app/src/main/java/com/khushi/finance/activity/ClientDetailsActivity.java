package com.khushi.finance.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.khushi.finance.R;
import com.khushi.finance.adapter.ChildAdapter;
import com.khushi.finance.adapter.DocumentAdapter;
import com.khushi.finance.adapter.FamilyAdapter;
import com.khushi.finance.adapter.PaymentDetalisAdapter;
import com.khushi.finance.model.ChildModel;
import com.khushi.finance.model.ClientManagementModel;
import com.khushi.finance.model.DocumentModel;
import com.khushi.finance.model.FamilyModel;
import com.khushi.finance.model.PaymentDetailsModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.DividerItemDecoration;
import com.khushi.finance.utils.FixAppBarLayoutBehavior;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.Services;
import com.khushi.finance.view.CircleImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ClientDetailsActivity extends AppCompatActivity {

    private CollapsingToolbarLayout collapsing_toolbar;
    private CircleImageView iv_client_img;
    private AppCompatTextView tv_client_type, tv_first_name, tv_middle_name, tv_last_name, tv_birthdate, tv_gender, tv_client_education, tv_nationality, tv_free_health_checkup, tv_health_checkup_year, tv_residential_status, tv_marital_status, tv_anniversary_date, tv_pan_number, tv_kyc_processed, tv_occupation, tv_occupation_other, tv_mobile, tv_phone_no, tv_email, tv_tax_status, tv_place_of_incorporation, tv_industry, tv_date_of_incorporation, tv_designation, tv_company_website, tv_address, tv_country, tv_state, tv_city, tv_pincode, tv_amount, tv_cheque_number, tv_cheque_date, tv_bank_name, tv_bank_branch, tv_mutual_fund, tv_insurance, tv_loan;
    private ClientManagementModel model;
    private RecyclerView rvDocuments, rvFamily, rvChild, rvPaymentDetails;
    private ArrayList<DocumentModel> arrayList;
    private ArrayList<PaymentDetailsModel> arrayListPaymentDetails;
    private ArrayList<FamilyModel> arrayListFamily;
    private ArrayList<ChildModel> arrayListChild;
    private DocumentAdapter adapter;
    private PaymentDetalisAdapter paymentDetalisAdapter;
    private FamilyAdapter familyAdapter;
    private ChildAdapter childAdapter;
    private LinearLayout llClientType, llFirstName, llMiddleName, llLastName, llBirthDate, llGender, llClientEducation, llNationality, llFreeHealthCheckup, llHealthCheckupYear, llResidentialStatus, llMaritalStatus, llAnniversaryDate, llPanNumber, llKYCProcessed, llOccupation, llOccupationOther, llMobile, llPhoneNo, llEmail, llTaxStatus, llPlaceOfIncorporation, llIndustry, llDateOfIncorporation, llDesignation, llCompanyWebsite, llAddress, llCountry, llState, llCity, llPincode, llAmount, llChequeNumber, llChequeDate, llBankName, llBankBranch, llMutualFund, llInsurance, llLoan;
    private CardView cvFamily,cvChild, cvClientServices, cvPaymentDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_details);

        AppBarLayout abl = findViewById(R.id.appBarClientDetails);
        ((CoordinatorLayout.LayoutParams) abl.getLayoutParams()).setBehavior(new FixAppBarLayoutBehavior());

        collapsing_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsing_toolbar.setCollapsedTitleTextColor(Color.WHITE);
        collapsing_toolbar.setExpandedTitleColor(Color.TRANSPARENT);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rvDocuments = (RecyclerView) findViewById(R.id.rvDocuments);
        rvDocuments.setLayoutManager(new GridLayoutManager(ClientDetailsActivity.this, 3));
        rvDocuments.setHasFixedSize(true);

        rvPaymentDetails = (RecyclerView) findViewById(R.id.rvPaymentDetails);
        rvPaymentDetails.setLayoutManager(new LinearLayoutManager(ClientDetailsActivity.this));
        rvPaymentDetails.setHasFixedSize(true);

        rvFamily = (RecyclerView) findViewById(R.id.rvFamily);
        rvFamily.setLayoutManager(new LinearLayoutManager(ClientDetailsActivity.this));
        rvFamily.setHasFixedSize(true);

        rvChild = (RecyclerView) findViewById(R.id.rvChild);
        rvChild.setLayoutManager(new LinearLayoutManager(ClientDetailsActivity.this));
        rvChild.setHasFixedSize(true);

        llClientType = (LinearLayout) findViewById(R.id.llClientType);
        llFirstName = (LinearLayout) findViewById(R.id.llFirstName);
        llMiddleName = (LinearLayout) findViewById(R.id.llMiddleName);
        llLastName = (LinearLayout) findViewById(R.id.llLastName);
        llBirthDate = (LinearLayout) findViewById(R.id.llBirthDate);
        llGender = (LinearLayout) findViewById(R.id.llGender);
        llClientEducation = (LinearLayout) findViewById(R.id.llClientEducation);
        llNationality = (LinearLayout) findViewById(R.id.llNationality);
        llFreeHealthCheckup = (LinearLayout) findViewById(R.id.llFreeHealthCheckup);
        llHealthCheckupYear = (LinearLayout) findViewById(R.id.llHealthCheckupYear);
        llResidentialStatus = (LinearLayout) findViewById(R.id.llResidentialStatus);
        llMaritalStatus = (LinearLayout) findViewById(R.id.llMaritalStatus);
        llAnniversaryDate = (LinearLayout) findViewById(R.id.llAnniversaryDate);
        llPanNumber = (LinearLayout) findViewById(R.id.llPanNumber);
        llKYCProcessed = (LinearLayout) findViewById(R.id.llKYCProcessed);
        llOccupation = (LinearLayout) findViewById(R.id.llOccupation);
        llOccupationOther = (LinearLayout) findViewById(R.id.llOccupationOther);
        llMobile = (LinearLayout) findViewById(R.id.llMobile);
        llPhoneNo = (LinearLayout) findViewById(R.id.llPhoneNo);
        llEmail = (LinearLayout) findViewById(R.id.llEmail);
        llTaxStatus = (LinearLayout) findViewById(R.id.llTaxStatus);
        llPlaceOfIncorporation = (LinearLayout) findViewById(R.id.llPlaceOfIncorporation);
        llIndustry = (LinearLayout) findViewById(R.id.llIndustry);
        llDateOfIncorporation = (LinearLayout) findViewById(R.id.llDateOfIncorporation);
        llDesignation = (LinearLayout) findViewById(R.id.llDesignation);
        llCompanyWebsite = (LinearLayout) findViewById(R.id.llCompanyWebsite);
        llAddress = (LinearLayout) findViewById(R.id.llAddress);
        llCountry = (LinearLayout) findViewById(R.id.llCountry);
        llState = (LinearLayout) findViewById(R.id.llState);
        llCity = (LinearLayout) findViewById(R.id.llCity);
        llPincode = (LinearLayout) findViewById(R.id.llPincode);
        llAmount = (LinearLayout) findViewById(R.id.llAmount);
        llChequeNumber = (LinearLayout) findViewById(R.id.llChequeNumber);
        llChequeDate = (LinearLayout) findViewById(R.id.llChequeDate);
        llBankName = (LinearLayout) findViewById(R.id.llBankName);
        llBankBranch = (LinearLayout) findViewById(R.id.llBankBranch);
        llMutualFund = (LinearLayout) findViewById(R.id.llMutualFund);
        llInsurance = (LinearLayout) findViewById(R.id.llInsurance);
        llLoan = (LinearLayout) findViewById(R.id.llLoan);

        cvFamily = (CardView) findViewById(R.id.cvFamily);
        cvChild = (CardView) findViewById(R.id.cvChild);
        cvClientServices = (CardView) findViewById(R.id.cvClientServices);
        cvPaymentDetails = (CardView) findViewById(R.id.cvPaymentDetails);

        iv_client_img = (CircleImageView) findViewById(R.id.iv_client_img);
        tv_client_type = (AppCompatTextView) findViewById(R.id.tv_client_type);
        tv_first_name = (AppCompatTextView) findViewById(R.id.tv_first_name);
        tv_middle_name = (AppCompatTextView) findViewById(R.id.tv_middle_name);
        tv_last_name = (AppCompatTextView) findViewById(R.id.tv_last_name);
        tv_birthdate = (AppCompatTextView) findViewById(R.id.tv_birthdate);
        tv_gender = (AppCompatTextView) findViewById(R.id.tv_gender);
        tv_client_education = (AppCompatTextView) findViewById(R.id.tv_client_education);
        tv_nationality = (AppCompatTextView) findViewById(R.id.tv_nationality);
        tv_free_health_checkup = (AppCompatTextView) findViewById(R.id.tv_free_health_checkup);
        tv_health_checkup_year = (AppCompatTextView) findViewById(R.id.tv_health_checkup_year);
        tv_residential_status = (AppCompatTextView) findViewById(R.id.tv_residential_status);
        tv_marital_status = (AppCompatTextView) findViewById(R.id.tv_marital_status);
        tv_anniversary_date = (AppCompatTextView) findViewById(R.id.tv_anniversary_date);
        tv_pan_number = (AppCompatTextView) findViewById(R.id.tv_pan_number);
        tv_kyc_processed = (AppCompatTextView) findViewById(R.id.tv_kyc_processed);
        tv_occupation = (AppCompatTextView) findViewById(R.id.tv_occupation);
        tv_occupation_other = (AppCompatTextView) findViewById(R.id.tv_occupation_other);
        tv_mobile = (AppCompatTextView) findViewById(R.id.tv_mobile);
        tv_phone_no = (AppCompatTextView) findViewById(R.id.tv_phone_no);
        tv_email = (AppCompatTextView) findViewById(R.id.tv_email);
        tv_tax_status = (AppCompatTextView) findViewById(R.id.tv_tax_status);
        tv_place_of_incorporation = (AppCompatTextView) findViewById(R.id.tv_place_of_incorporation);
        tv_industry = (AppCompatTextView) findViewById(R.id.tv_industry);
        tv_date_of_incorporation = (AppCompatTextView) findViewById(R.id.tv_date_of_incorporation);
        tv_designation = (AppCompatTextView) findViewById(R.id.tv_designation);
        tv_company_website = (AppCompatTextView) findViewById(R.id.tv_company_website);
        tv_address = (AppCompatTextView) findViewById(R.id.tv_address);
        tv_country = (AppCompatTextView) findViewById(R.id.tv_country);
        tv_state = (AppCompatTextView) findViewById(R.id.tv_state);
        tv_city = (AppCompatTextView) findViewById(R.id.tv_city);
        tv_pincode = (AppCompatTextView) findViewById(R.id.tv_pincode);
        tv_amount = (AppCompatTextView) findViewById(R.id.tv_amount);
        tv_cheque_number = (AppCompatTextView) findViewById(R.id.tv_cheque_number);
        tv_cheque_date = (AppCompatTextView) findViewById(R.id.tv_cheque_date);
        tv_bank_name = (AppCompatTextView) findViewById(R.id.tv_bank_name);
        tv_bank_branch = (AppCompatTextView) findViewById(R.id.tv_bank_branch);
        tv_mutual_fund = (AppCompatTextView) findViewById(R.id.tv_mutual_fund);
        tv_insurance = (AppCompatTextView) findViewById(R.id.tv_insurance);
        tv_loan = (AppCompatTextView) findViewById(R.id.tv_loan);

        if (getIntent() != null) {
            model = (ClientManagementModel) getIntent().getSerializableExtra("client");

            process(model.getClientType(), tv_client_type, llClientType);
            process(model.getFirstName(), tv_first_name, llFirstName);
            process(model.getMiddleName(), tv_middle_name, llMiddleName);
            process(model.getLastName(), tv_last_name, llLastName);
            process(model.getClientDob(), tv_birthdate, llBirthDate);
            process(model.getGender(), tv_gender, llGender);
            process(model.getClientEducation(), tv_client_education, llClientEducation);
            process(model.getNationality(), tv_nationality, llNationality);
            process(model.getFreeHealthCheckup(), tv_free_health_checkup, llFreeHealthCheckup);
            process(model.getHealthCheckupYear(), tv_health_checkup_year, llHealthCheckupYear);
            process(model.getMutualFund(), tv_mutual_fund, llMutualFund);
            process(model.getInsurance(), tv_insurance, llInsurance);
            process(model.getLoan(), tv_loan, llLoan);
            process(model.getResidentialStatus(), tv_residential_status, llResidentialStatus);
            process(model.getMeritalStatus(), tv_marital_status, llMaritalStatus);
            process(model.getAnniversaryDate(), tv_anniversary_date, llAnniversaryDate);
            process(model.getPanNumber(), tv_pan_number, llPanNumber);
            process(model.getKycProcessed(), tv_kyc_processed, llKYCProcessed);
            process(model.getOccupation(), tv_occupation, llOccupation);
            process(model.getOccupationOther(), tv_occupation_other, llOccupationOther);
            process(model.getMobile(), tv_mobile, llMobile);
            process(model.getPhoneNo(), tv_phone_no, llPhoneNo);
            process(model.getEmail(), tv_email, llEmail);
            process(model.getTaxStatus(), tv_tax_status, llTaxStatus);
            process(model.getPlaceOfIncorporation(), tv_place_of_incorporation, llPlaceOfIncorporation);
            process(model.getIndustry(), tv_industry, llIndustry);
            process(model.getDateOfIncorporation(), tv_date_of_incorporation, llDateOfIncorporation);
            process(model.getDesignation(), tv_designation, llDesignation);
            process(model.getCompanyWebsite(), tv_company_website, llCompanyWebsite);
            process(model.getAddress(), tv_address, llAddress);
            process(model.getCountry(), tv_country, llCountry);
            process(model.getState(), tv_state, llState);
            process(model.getCity(), tv_city, llCity);
            process(model.getPincode(), tv_pincode, llPincode);
            process(model.getAmount(), tv_amount, llAmount);
            process(model.getChequeNumber(), tv_cheque_number, llChequeNumber);
            process(model.getChequeDate(), tv_cheque_date, llChequeDate);
            process(model.getBankName(), tv_bank_name, llBankName);
            process(model.getBankBranch(), tv_bank_branch, llBankBranch);

            Picasso.get()
                    .load(model.getUserPhoto())
                    .resize(250, 250)
                    .centerCrop()
                    .error(R.drawable.image_not_available)
                    .into(iv_client_img);

            getDocuments(model.getClientId());
            getPaymentDetails(model.getClientId());

            if(model.getClientType().toLowerCase().equalsIgnoreCase("non-individual")){
                cvFamily.setVisibility(View.GONE);
                cvChild.setVisibility(View.GONE);
                cvClientServices.setVisibility(View.GONE);
            } else{
                arrayListFamily = model.getFamilyModelArrayList();
                familyAdapter = new FamilyAdapter(ClientDetailsActivity.this,arrayListFamily);
                rvFamily.setAdapter(familyAdapter);
                rvFamily.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(ClientDetailsActivity.this, R.drawable.rv_divider)));

                arrayListChild = model.getChildArrayList();
                childAdapter = new ChildAdapter(ClientDetailsActivity.this,arrayListChild);
                rvChild.setAdapter(childAdapter);
                rvChild.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(ClientDetailsActivity.this, R.drawable.rv_divider)));
            }
        }
    }

    public void process(String value, AppCompatTextView textView, LinearLayout linearLayout) {
        if (TextUtils.isEmpty(value)) {
            linearLayout.setVisibility(View.GONE);
        } else {
            textView.setText(value);
        }
    }

    private void getDocuments(String clientId) {

        arrayList = new ArrayList<>();

        if (Helper.isNetworkAvailable(ClientDetailsActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get Document : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                DocumentModel model = new DocumentModel();
                                model.setDocument_id(innerObj.getString("document_id"));
                                model.setUser_document_image(innerObj.getString("user_document_image"));

                                arrayList.add(model);
                            }
                            adapter = new DocumentAdapter(ClientDetailsActivity.this, arrayList);
                            rvDocuments.setAdapter(adapter);
                        } else {
                            //Helper.showToast(ClientDetailsActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(ClientDetailsActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("client_id", model.getClientId());

            API api = new API(ClientDetailsActivity.this, apiResponse);
            api.execute(1, Services.GET_CLIENT_DOCUMENT, params, true);
        } else {
            Helper.showToast(ClientDetailsActivity.this, Services.NO_NETWORK);
        }
    }


    private void getPaymentDetails(String clientId) {

        arrayListPaymentDetails = new ArrayList<>();

        if (Helper.isNetworkAvailable(ClientDetailsActivity.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get Payment Details : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                PaymentDetailsModel paymentDetailsModel = new PaymentDetailsModel();
                                paymentDetailsModel.setClient_payment_id(innerObj.getString("client_payment_id"));
                                paymentDetailsModel.setClient_id(innerObj.getString("client_id"));
                                paymentDetailsModel.setPayment_date(innerObj.getString("payment_date"));
                                paymentDetailsModel.setAmount(innerObj.getString("amount"));
                                paymentDetailsModel.setCheque_number(innerObj.getString("cheque_number"));
                                paymentDetailsModel.setCheque_date(innerObj.getString("cheque_date"));
                                paymentDetailsModel.setBank_name(innerObj.getString("bank_name"));
                                paymentDetailsModel.setBank_branch(innerObj.getString("bank_branch"));
                                paymentDetailsModel.setClient_note(innerObj.getString("client_note"));
                                paymentDetailsModel.setPayment_create_time(innerObj.getString("payment_create_time"));

                                arrayListPaymentDetails.add(paymentDetailsModel);
                            }
                            paymentDetalisAdapter = new PaymentDetalisAdapter(ClientDetailsActivity.this, arrayListPaymentDetails);
                            rvPaymentDetails.setAdapter(paymentDetalisAdapter);
                        } else {
                            //Helper.showToast(ClientDetailsActivity.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(ClientDetailsActivity.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("client_id", model.getClientId());

            API api = new API(ClientDetailsActivity.this, apiResponse);
            api.execute(2, Services.GET_PAYMENT_DETAILS, params, true);
        } else {
            Helper.showToast(ClientDetailsActivity.this, Services.NO_NETWORK);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
