package com.khushi.finance.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.adapter.InquiryImageAdapter;
import com.khushi.finance.adapter.ServicesAdapter;
import com.khushi.finance.model.InquiryModel;
import com.khushi.finance.model.ServicesModel;
import com.khushi.finance.utils.Helper;

import java.util.ArrayList;

public class InquiryDetailsActivity extends AppCompatActivity {

    private TextView tv_name, tv_email, tv_phone, tv_message, tv_created_time,tv_assigned_time;
    private RecyclerView rvServices, rvImages;
    private InquiryModel model;
    private String sel_service;
    private ArrayList<String> arrayListImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inquiry_details);
        initView();

        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar);
        toolbarTitle.setText("Details");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (getIntent() != null) {
            model = (InquiryModel) getIntent().getSerializableExtra("inquiry");
            tv_name.setText("Name : " + model.getName());
            tv_email.setText("Email : " + model.getEmail());
            tv_phone.setText("Phone : " + model.getPhone());
            tv_message.setText("Message : " + model.getMessage());
            tv_created_time.setText("Created at : "+model.getCreatedTime());
            /*if(model.getAssignedTime().length()>0){
                tv_assigned_time.setVisibility(View.VISIBLE);
                tv_assigned_time.setText("Assigned at : "+model.getAssignedTime());
            }*/


            sel_service = model.getServices_name();

            ArrayList<ServicesModel> arrayList = Helper.servicesArrayList;
            ArrayList<String> requiredServices = new ArrayList<>();
            ArrayList<ServicesModel> adapterServices = new ArrayList<>();

            String[] serviceArray = sel_service.split(",");
            for (int x = 0; x < serviceArray.length; x++) {
                requiredServices.add(serviceArray[x]);
            }

            /******************** THIS WILL SHOW ALL SERVICES ********************/
            /*for (int i = 0; i < arrayList.size(); i++) {
                ServicesModel model = new ServicesModel();
                model.setServiceId(arrayList.get(i).getServiceId());
                model.setServiceName(arrayList.get(i).getServiceName());
                if (requiredServices.contains(arrayList.get(i).getServiceId())) {
                    model.setSelected(true);
                } else {
                    model.setSelected(false);
                }

                adapterServices.add(model);
            }*/

            /******************** THIS WILL SHOW ONLY SELECTED SERVICES ********************/
            for (int i = 0; i < arrayList.size(); i++) {
                ServicesModel model = new ServicesModel();
                model.setServiceId(arrayList.get(i).getServiceId());
                model.setServiceName(arrayList.get(i).getServiceName());
                if (requiredServices.contains(arrayList.get(i).getServiceId())) {
                    model.setSelected(true);
                    adapterServices.add(model);
                }
            }
            rvServices.setAdapter(new ServicesAdapter(getApplicationContext(), adapterServices));

            if (!TextUtils.isEmpty(model.getImage1())) {
                arrayListImg.add(model.getImage1());
            }

            if (!TextUtils.isEmpty(model.getImage2())) {
                arrayListImg.add(model.getImage2());
            }

            if (!TextUtils.isEmpty(model.getImage3())) {
                arrayListImg.add(model.getImage3());
            }

            if (!TextUtils.isEmpty(model.getImage4())) {
                arrayListImg.add(model.getImage4());
            }

            if (!TextUtils.isEmpty(model.getImage5())) {
                arrayListImg.add(model.getImage5());
            }

            rvImages.setAdapter(new InquiryImageAdapter(InquiryDetailsActivity.this, arrayListImg));
        }
    }

    private void initView() {
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_email = (TextView) findViewById(R.id.tv_email);
        tv_phone = (TextView) findViewById(R.id.tv_phone);
        tv_message = (TextView) findViewById(R.id.tv_message);
        tv_created_time = (TextView) findViewById(R.id.tv_created_time);
        tv_assigned_time = (TextView) findViewById(R.id.tv_assigned_time);

        rvServices = (RecyclerView) findViewById(R.id.rvServices);
        rvServices.setLayoutManager(new GridLayoutManager(InquiryDetailsActivity.this, 2));
        rvServices.setHasFixedSize(true);

        rvImages = (RecyclerView) findViewById(R.id.rvImages);
        rvImages.setLayoutManager(new GridLayoutManager(InquiryDetailsActivity.this, 3));
        rvImages.setHasFixedSize(true);

        arrayListImg = new ArrayList<>();
    }
}
