package com.khushi.finance.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.adapter.ToDoEmployeeAdapter;
import com.khushi.finance.model.ToDoEmployeeModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AssignToDo extends AppCompatActivity {

    private AppCompatSpinner spnEmployee;
    private ToDoEmployeeAdapter toDoEmployeeAdapter;
    private ArrayList<ToDoEmployeeModel> arrayList;
    private String user_id, designation, login_status;
    private EditText etMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_to_do);

        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar);
        toolbarTitle.setText("Assign To Do");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.hideSoftKeyboard(AssignToDo.this,v);
                onBackPressed();
            }
        });

        spnEmployee = (AppCompatSpinner) findViewById(R.id.spnEmployee);
        etMessage = (EditText) findViewById(R.id.etMessage);

        SharedPreferences preferences = getApplicationContext().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        user_id = preferences.getString("user_id", "");
        login_status = preferences.getString("login_status", "");
        designation = preferences.getString("designation", "");

        getToDoEmployee();
    }

    private void getToDoEmployee() {
        arrayList = new ArrayList<>();

        ToDoEmployeeModel model = new ToDoEmployeeModel();

        model.setUserId("-1");
        model.setUserName("Select Employee");
        model.setUserFirstname("Select");
        model.setUserLastname("Employee");

        arrayList.add(model);

        if (Helper.isNetworkAvailable(AssignToDo.this)) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get ToDo Employee : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                ToDoEmployeeModel model = new ToDoEmployeeModel();

                                model.setUserId(innerObj.getString("user_id"));
                                model.setUserName(innerObj.getString("user_name"));
                                model.setUserFirstname(innerObj.getString("user_firstname"));
                                model.setUserLastname(innerObj.getString("user_lastname"));

                                arrayList.add(model);
                            }

                            toDoEmployeeAdapter = new ToDoEmployeeAdapter(AssignToDo.this, arrayList);
                            spnEmployee.setAdapter(toDoEmployeeAdapter);
                        } else {
                            Helper.showToast(AssignToDo.this, message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(AssignToDo.this, error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_id);
            params.put("designation", designation);

            API api = new API(AssignToDo.this, apiResponse);
            api.execute(1, Services.GET_TODO_EMPLOYEE, params, true);
        } else {
            Helper.showToast(AssignToDo.this, Services.NO_NETWORK);
        }
    }

    public void onAssignToDo(View view) {

        Helper.hideSoftKeyboard(AssignToDo.this,view);

        String selectedId = arrayList.get(spnEmployee.getSelectedItemPosition()).getUserId();
        String message = etMessage.getText().toString().trim();

        if(selectedId.equalsIgnoreCase("-1")){
            Helper.showToast(AssignToDo.this,"Please, Select Employee");
        } else if(TextUtils.isEmpty(message)){
            Helper.showToast(AssignToDo.this,"Please, Enter To Do");
        } else {
            if (Helper.isNetworkAvailable(AssignToDo.this)) {
                APIResponse apiResponse = new APIResponse() {
                    @Override
                    public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                        try {
                            Helper.showLog("Add ToDo : " + response);
                            JSONObject object = new JSONObject(response);
                            String status = object.getString("status");
                            String message = object.getString("message");
                            if (status.equalsIgnoreCase("true")) {
                                Helper.showToast(AssignToDo.this, message);
                                onBackPressed();
                            } else {
                                Helper.showToast(AssignToDo.this, message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onAPIError(int requestCode, boolean isError, String error) {
                        if (isError) {
                            Helper.showToast(AssignToDo.this, error);
                        }
                    }
                };

                HashMap<String, String> params = new HashMap<>();
                params.put("user_id", user_id);
                params.put("login_status", login_status);
                params.put("employee_id", selectedId);
                params.put("message", message);

                Log.e("EMPLOYEE ID",selectedId);
                /*if (!designation.toLowerCase().equalsIgnoreCase("admin")) {
                    params.put("user_id", user_id);
                }
                params.put("designation", designation);*/

                API api = new API(AssignToDo.this, apiResponse);
                api.execute(1, Services.ADD_TODO, params, true);
            } else {
                Helper.showToast(AssignToDo.this, Services.NO_NETWORK);
            }
        }
    }
}
