package com.khushi.finance.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.khushi.finance.R;
import com.khushi.finance.activity.AssignToDo;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.activity.ToDoDetails;
import com.khushi.finance.adapter.ToDoAdapter;
import com.khushi.finance.model.ToDoModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.RecyclerItemClickListener;
import com.khushi.finance.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;

public class ToDoFragment extends Fragment {
    private FloatingActionButton fabAddTodo;
    private RecyclerView rvTodo;
    private ToDoAdapter toDoAdapter;
    private ArrayList<ToDoModel> arrayList, arrayListFinal, arrayListCompleted, arrayListInComplete;
    private ConstraintLayout clNoData;
    private String user_id;
    private String login_status;
    private SearchView searchView;
    private int checkedItem = -1;
    private String[] filterTypes;
    private int RESULT_ADD_TODO = 54;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).disableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("To Do");

        clNoData = (ConstraintLayout) view.findViewById(R.id.clNoData);

        fabAddTodo = (FloatingActionButton) view.findViewById(R.id.fabAddTodo);
        fabAddTodo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), AssignToDo.class),RESULT_ADD_TODO);
            }
        });

        rvTodo = (RecyclerView) view.findViewById(R.id.rvTodo);
        rvTodo.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTodo.setHasFixedSize(true);

        filterTypes = getResources().getStringArray(R.array.todo_filter);

        SharedPreferences preferences = getActivity().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        user_id = preferences.getString("user_id", "");
        login_status = preferences.getString("login_status", "");

        getToDo();

        rvTodo.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), rvTodo, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), ToDoDetails.class);
                intent.putExtra("todo", arrayListFinal.get(position));
                intent.putExtra("showdonebutton", false);
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        return view;
    }

    private void getToDo() {

        arrayList = new ArrayList<>();
        arrayListFinal = new ArrayList<>();
        arrayListCompleted = new ArrayList<>();
        arrayListInComplete = new ArrayList<>();

        arrayList.clear();
        arrayListFinal.clear();
        arrayListCompleted.clear();
        arrayListInComplete.clear();

        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get ToDo : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                ToDoModel model = new ToDoModel();
                                model.setTodoId(innerObj.getString("todo_id"));
                                model.setTodoMessage(innerObj.getString("todo_message").trim());
                                model.setTodoStatus(innerObj.getString("todo_status"));
                                model.setEmployeeId(innerObj.getString("employee_id"));
                                model.setEmployeeUsername(innerObj.getString("employee_username"));
                                model.setEmployeeFirstname(innerObj.getString("employee_firstname"));
                                model.setEmployeeLastname(innerObj.getString("employee_lastname"));
                                model.setEmployeeDesignation(innerObj.getString("employee_designation"));
                                if(innerObj.getString("employee_id").equalsIgnoreCase(user_id)){
                                    if(innerObj.has("to_name")){
                                        model.setTodoTo("Assigned to "+innerObj.getString("to_name"));
                                    } else {
                                        model.setTodoTo("Assigned to ");
                                    }
                                } else {
                                    model.setTodoTo("Assigned to Me");
                                }

                                arrayList.add(model);

                                if (innerObj.getString("todo_status").equalsIgnoreCase("0")) {
                                    arrayListInComplete.add(model);
                                } else if (innerObj.getString("todo_status").equalsIgnoreCase("1")) {
                                    arrayListCompleted.add(model);
                                }
                            }
                            arrayListFinal = arrayList;

                            if(arrayList.size()>0){
                                clNoData.setVisibility(View.GONE);
                            }
                        } else {
                            clNoData.setVisibility(View.VISIBLE);
                        }

                        toDoAdapter = new ToDoAdapter(getActivity(), arrayList);
                        rvTodo.setAdapter(toDoAdapter);
                        toDoAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            /*if (!designation.toLowerCase().equalsIgnoreCase("admin")) {
                params.put("user_id", user_id);
            } else {
                params.put("designation", designation);
            }*/
            params.put("user_id", user_id);
            params.put("login_status",login_status);

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_TODO, params, true);
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_todo, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, 10);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setMaxWidth(android.R.attr.width);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            public boolean onQueryTextChange(String newText) {
                arrayListFinal = new ArrayList<>();

                if (TextUtils.isEmpty(newText)) {
                    arrayListFinal.addAll(arrayList);
                } else {
                    for (ToDoModel item : arrayList) {
                        if (item.getEmployeeUsername().toLowerCase().contains(newText.toLowerCase())) {
                            arrayListFinal.add(item);
                        }
                    }
                }

                toDoAdapter = new ToDoAdapter(getActivity(), arrayListFinal);
                rvTodo.setAdapter(toDoAdapter);
                toDoAdapter.notifyDataSetChanged();

                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                filter();
                break;
        }
        return true;
    }

    private void filter() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle((CharSequence) "Select Filter");
        builder.setSingleChoiceItems(filterTypes, checkedItem, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                checkedItem = item;
            }
        });
        builder.setPositiveButton((CharSequence) "Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (checkedItem == 0) {
                    arrayListFinal = arrayList;
                    toDoAdapter = new ToDoAdapter(getActivity(), arrayListFinal);
                    rvTodo.setAdapter(toDoAdapter);
                    toDoAdapter.notifyDataSetChanged();
                } else if (checkedItem == 1) {
                    arrayListFinal = arrayListCompleted;
                    toDoAdapter = new ToDoAdapter(getActivity(), arrayListFinal);
                    rvTodo.setAdapter(toDoAdapter);
                    toDoAdapter.notifyDataSetChanged();
                } else if (checkedItem == 2) {
                    arrayListFinal = arrayListInComplete;
                    toDoAdapter = new ToDoAdapter(getActivity(), arrayListFinal);
                    rvTodo.setAdapter(toDoAdapter);
                    toDoAdapter.notifyDataSetChanged();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_ADD_TODO) {
            getToDo();
        }
    }
}
