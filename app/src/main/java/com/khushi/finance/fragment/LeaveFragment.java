package com.khushi.finance.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.khushi.finance.R;
import com.khushi.finance.activity.LeaveDetailsActivity;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.activity.RequestLeaveActivity;
import com.khushi.finance.adapter.LeaveAdapter;
import com.khushi.finance.model.LeaveModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.RecyclerItemClickListener;
import com.khushi.finance.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;

public class LeaveFragment extends Fragment {

    private FloatingActionButton fabAddLeave;
    private RecyclerView rvLeave;
    private LeaveAdapter leaveAdapter;
    private ArrayList<LeaveModel> arrayList,arrayListFinal;
    private ConstraintLayout clNoData;
    private String user_id, designation;
    private SearchView searchView;
    private int RESULT_REQUEST_LEAVE = 53;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_leave, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).disableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("Leave");

        clNoData = (ConstraintLayout) view.findViewById(R.id.clNoData);

        fabAddLeave = (FloatingActionButton) view.findViewById(R.id.fabAddLeave);
        fabAddLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), RequestLeaveActivity.class),RESULT_REQUEST_LEAVE);
            }
        });

        rvLeave = (RecyclerView) view.findViewById(R.id.rvLeave);
        rvLeave.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvLeave.setHasFixedSize(true);

        SharedPreferences preferences = getActivity().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        user_id = preferences.getString("user_id", "");
        designation = preferences.getString("designation", "");

        if (designation.toLowerCase().equalsIgnoreCase("admin")) {
            fabAddLeave.setVisibility(View.GONE);
        }

        getLeave();

        rvLeave.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), rvLeave, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), LeaveDetailsActivity.class);
                intent.putExtra("leave", arrayList.get(position));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        return view;
    }

    private void getLeave() {

        arrayList = new ArrayList<>();
        arrayListFinal = new ArrayList<>();

        arrayList.clear();
        arrayListFinal.clear();

        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get Leave : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                LeaveModel leaveModel = new LeaveModel();

                                leaveModel.setUserId(innerObj.getString("user_id"));
                                leaveModel.setUserName(innerObj.getString("user_name"));
                                leaveModel.setLeaveId(innerObj.getString("leave_id"));
                                leaveModel.setStartDate(new Helper().formattedDate(innerObj.getString("start_date")));
                                leaveModel.setEndDate(new Helper().formattedDate(innerObj.getString("end_date")));
                                leaveModel.setHours(innerObj.getString("hours"));
                                leaveModel.setReasonToLeave(innerObj.getString("reason_to_leave"));
                                leaveModel.setLeaveCreateTime(innerObj.getString("leave_create_time"));
                                leaveModel.setStatus(innerObj.getString("status"));
                                leaveModel.setReasonToApprove(innerObj.getString("reason_to_approve"));

                                arrayList.add(leaveModel);
                            }

                            arrayListFinal = arrayList;

                            if(arrayList.size()>0){
                                clNoData.setVisibility(View.GONE);
                            }
                        } else {
                            clNoData.setVisibility(View.VISIBLE);
                        }

                        leaveAdapter = new LeaveAdapter(getActivity(), arrayList);
                        rvLeave.setAdapter(leaveAdapter);
                        leaveAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            if (!designation.toLowerCase().equalsIgnoreCase("admin")) {
                params.put("user_id", user_id);
            }
            params.put("designation", designation);

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_LEAVE, params, true);
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }

    //https://en.proft.me/2018/01/2/how-filter-recyclerview-searchview-toolbar/
    //http://www.coderzheaven.com/2016/05/13/filtering-a-recyclerview-with-custom-objects/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_leave, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, 10);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setMaxWidth(android.R.attr.width);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            public boolean onQueryTextChange(String newText) {
                arrayListFinal = new ArrayList<>();

                if (TextUtils.isEmpty(newText)) {
                    arrayListFinal.addAll(arrayList);
                } else {
                    for (LeaveModel item : arrayList) {
                        if (item.getUserName().toLowerCase().contains(newText.toLowerCase())) {
                            arrayListFinal.add(item);
                        }
                    }
                }

                leaveAdapter = new LeaveAdapter(getActivity(), arrayListFinal);
                rvLeave.setAdapter(leaveAdapter);
                leaveAdapter.notifyDataSetChanged();

                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_REQUEST_LEAVE) {
            getLeave();
        }
    }
}
