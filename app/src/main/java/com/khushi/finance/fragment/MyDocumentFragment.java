package com.khushi.finance.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khushi.finance.R;
import com.khushi.finance.activity.ClientDetailsActivity;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.adapter.DocumentAdapter;
import com.khushi.finance.model.DocumentModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;

public class MyDocumentFragment extends Fragment {

    private RecyclerView rvDocuments;
    private ArrayList<DocumentModel> arrayList;
    private DocumentAdapter adapter;
    private ConstraintLayout clNoData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_document,container,false);
        ((MainActivity) getActivity()).disableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("My Documents");

        clNoData = (ConstraintLayout) view.findViewById(R.id.clNoData);

        rvDocuments = (RecyclerView) view.findViewById(R.id.rvDocuments);
        rvDocuments.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rvDocuments.setHasFixedSize(true);

        getDocuments();

        return view;
    }

    private void getDocuments() {
        SharedPreferences preferences = getActivity().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        String user_id = preferences.getString("user_id", "");

        arrayList = new ArrayList<>();

        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get Document : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                DocumentModel model = new DocumentModel();
                                model.setDocument_id(innerObj.getString("document_id"));
                                model.setUser_document_image(innerObj.getString("user_document_image"));

                                arrayList.add(model);
                            }
                            adapter = new DocumentAdapter(getActivity(), arrayList);
                            rvDocuments.setAdapter(adapter);
                        } else {
                            clNoData.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("client_id", user_id);

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_CLIENT_DOCUMENT, params, true);
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }
}
