package com.khushi.finance.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khushi.finance.R;
import com.khushi.finance.activity.CreateInquiryActivity;
import com.khushi.finance.activity.LoginActivity;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.activity.ProfileActivity;
import com.khushi.finance.adapter.MenuAdapter;
import com.khushi.finance.model.MenuModel;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.RecyclerItemClickListener;
import com.khushi.finance.utils.Services;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment {

    private final int LOGIN_CODE = 9;
    private RecyclerView rvContainerMenu;
    private ArrayList<MenuModel> arrayList;
    private MenuAdapter menuAdapter;
    private SharedPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        preferences = getActivity().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);

        ((MainActivity) getActivity()).enableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("Home");

        rvContainerMenu = (RecyclerView) view.findViewById(R.id.rvContainerMenu);
        rvContainerMenu.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rvContainerMenu.setHasFixedSize(true);

        rvContainerMenu.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), rvContainerMenu, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (arrayList.get(position).getMenuTitle()) {
                    case "Home":
                        ((MainActivity) getActivity()).changeFragment(new HomeFragment());
                        break;

                    case "Contact Us":
                        ((MainActivity) getActivity()).changeFragment(new ContactUsFragment());
                        break;

                    case "Setting":
                        ((MainActivity) getActivity()).changeFragment(new SettingFragment());
                        break;

                    case "Inquiry Manager":
                        String email_id = preferences.getString("email_id", "");
                        if (email_id.length() > 0) {
                            ((MainActivity) getActivity()).changeFragment(new InquiryFragment());
                        } else {
                            startActivity(new Intent(getActivity(), CreateInquiryActivity.class));
                        }
                        break;

                    case "Client Login":
                        getActivity().startActivityForResult(new Intent(getActivity(), LoginActivity.class), LOGIN_CODE);
                        break;

                    case "Employee Login":
                        getActivity().startActivityForResult(new Intent(getActivity(), LoginActivity.class), LOGIN_CODE);
                        break;

                    case "Logout":
                        clearPreferences();
                        break;

                    case "Leave":
                        ((MainActivity) getActivity()).changeFragment(new LeaveFragment());
                        break;

                    case "Our Website":
                        new Helper().ourWebsite(getActivity());
                        break;

                    case "Leave Management": // For Admin
                        ((MainActivity) getActivity()).changeFragment(new LeaveFragment());
                        break;

                    case "Client Management":
                        ((MainActivity) getActivity()).changeFragment(new ClientManagementFragment());
                        break;

                    case "Manage Employee":
                        ((MainActivity) getActivity()).changeFragment(new ManageEmployeeFragment());
                        break;

                    case "Manage News":
                        ((MainActivity) getActivity()).changeFragment(new NewsFragment());
                        break;

                    case "Profile":
                        startActivity(new Intent(getActivity(), ProfileActivity.class));
                        break;

                    case "DCR (Daily Call Record)":
                        String email_id1 = preferences.getString("email_id", "");
                        if (email_id1.length() > 0) {
                            ((MainActivity) getActivity()).changeFragment(new InquiryFragment());
                        } else {
                            startActivity(new Intent(getActivity(), CreateInquiryActivity.class));
                        }
                        break;

                    case "To Do":
                        ((MainActivity) getActivity()).changeFragment(new ToDoFragment());
                        break;

                    case "My Document":
                        ((MainActivity) getActivity()).changeFragment(new MyDocumentFragment());
                        break;

                    case "My Leave":
                        ((MainActivity) getActivity()).changeFragment(new MyLeaveFragment());
                        break;

                    case "Lead Management":
                        ((MainActivity) getActivity()).changeFragment(new LeadFragment());
                        break;

                    case "Live":
                        ((MainActivity) getActivity()).changeFragment(new LiveFragment());
                        break;

                    case "Assigned To Do":
                        ((MainActivity) getActivity()).changeFragment(new ToDoAssignedToMeFragment());
                        break;
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateDrawerMenu();
    }

    private void clearPreferences() {
        SharedPreferences preferences = getActivity().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        updateDrawerMenu();
    }

    public void updateDrawerMenu() {
        SharedPreferences preferences = getActivity().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        String designation = preferences.getString("designation", "");

        switch (designation) {

            case "admin":
                arrayList = new Helper().getAdminMenu();
                menuAdapter = new MenuAdapter(getActivity(), arrayList, 2);
                rvContainerMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;

            case "Salesman":
                arrayList = new Helper().getSalesmanMenu();
                menuAdapter = new MenuAdapter(getActivity(), arrayList, 2);
                rvContainerMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;

            case "Staff":
                arrayList = new Helper().getStaffMenu();
                menuAdapter = new MenuAdapter(getActivity(), arrayList, 2);
                rvContainerMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;

            case "client":
                arrayList = new Helper().getRegisteredClientMenu();
                menuAdapter = new MenuAdapter(getActivity(), arrayList, 2);
                rvContainerMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;

            case "super_admin":
                arrayList = new Helper().getAdminMenu();
                menuAdapter = new MenuAdapter(getActivity(), arrayList, 2);
                rvContainerMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;

            default:
                arrayList = new Helper().getUnregisteredClientMenu();
                menuAdapter = new MenuAdapter(getActivity(), arrayList, 2);
                rvContainerMenu.setAdapter(menuAdapter);
                menuAdapter.notifyDataSetChanged();
                break;

        }

        ((MainActivity) getActivity()).updateDrawerMenu();
    }
}
