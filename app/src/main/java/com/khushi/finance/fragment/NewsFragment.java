package com.khushi.finance.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khushi.finance.R;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.activity.NewsDetailsActivity;
import com.khushi.finance.adapter.NewsAdapter;
import com.khushi.finance.model.NewsModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.RecyclerItemClickListener;
import com.khushi.finance.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NewsFragment extends Fragment {

    private ConstraintLayout clNoData;
    private RecyclerView rvNews;
    private ArrayList<NewsModel> arrayList;
    private NewsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        ((MainActivity) getActivity()).disableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("News");

        clNoData = (ConstraintLayout) view.findViewById(R.id.clNoData);

        rvNews = (RecyclerView) view.findViewById(R.id.rvNews);
        rvNews.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvNews.setHasFixedSize(true);

        getNews();

        rvNews.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), rvNews, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), NewsDetailsActivity.class);
                intent.putExtra("news", arrayList.get(position));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        return view;
    }

    private void getNews() {

        arrayList = new ArrayList<>();

        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get News : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                NewsModel model = new NewsModel();
                                model.setNewsId(innerObj.getString("news_id"));
                                model.setNewsTitle(innerObj.getString("news_title"));
                                model.setNewsDescription(innerObj.getString("news_description"));
                                model.setNewsDate(new Helper().formattedDateTime(innerObj.getString("news_date")));
                                arrayList.add(model);
                            }

                            adapter = new NewsAdapter(getActivity(), arrayList);
                            rvNews.setAdapter(adapter);
                        } else {
                            clNoData.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_NEWS, params, true);
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }
}
