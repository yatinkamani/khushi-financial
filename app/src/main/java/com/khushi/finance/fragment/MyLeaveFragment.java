package com.khushi.finance.fragment;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.khushi.finance.R;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.model.MyLeaveModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class MyLeaveFragment extends Fragment {

    private CompactCalendarView ccvMyLeave;
    private TextView tv_month,tvTotalLeave;
    private ImageView iv_prev, iv_next;
    private ArrayList<MyLeaveModel> arrayList;
    private SimpleDateFormat yearMonth = new SimpleDateFormat("yyyy-MM");
    private int totalLeave = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_leave, container, false);
        ((MainActivity) getActivity()).disableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("My Leave");

        tv_month = (TextView) view.findViewById(R.id.tv_month);
        iv_prev = (ImageView) view.findViewById(R.id.iv_prev);
        iv_next = (ImageView) view.findViewById(R.id.iv_next);
        tvTotalLeave = (TextView) view.findViewById(R.id.tvTotalLeave);

        ccvMyLeave = (CompactCalendarView) view.findViewById(R.id.ccvMyLeave);
        ccvMyLeave.setUseThreeLetterAbbreviation(true);
        ccvMyLeave.shouldDrawIndicatorsBelowSelectedDays(true);
        ccvMyLeave.setEventIndicatorStyle(3);

        ccvMyLeave.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                List<Event> events = ccvMyLeave.getEvents(dateClicked.getTime());
                if(events.size() > 0){
                    if(events.get(0).getData() instanceof MyLeaveModel){
                        MyLeaveModel model = (MyLeaveModel) events.get(0).getData();
                        //Helper.showLog("Event Details : "+model.getReasonToLeave());
                    }
                }
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                ccvMyLeave.removeAllEvents();

                SimpleDateFormat targetFormat = new SimpleDateFormat("MMM yyyy");
                tv_month.setText(targetFormat.format(firstDayOfNewMonth));

                getMyLeave(yearMonth.format(firstDayOfNewMonth));
            }
        });

        iv_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ccvMyLeave.scrollLeft();
            }
        });

        iv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ccvMyLeave.scrollRight();
            }
        });

        SimpleDateFormat df = new SimpleDateFormat("MMM yyyy");
        String date = df.format(Calendar.getInstance().getTime());
        tv_month.setText(date);

        getMyLeave(yearMonth.format(Calendar.getInstance().getTime()));

        return view;
    }

    private void getMyLeave(String month) {

        totalLeave = 0;
        arrayList = new ArrayList<>();

        SharedPreferences preferences = getActivity().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);
        String user_id = preferences.getString("user_id", "");

        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get My Leave : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                MyLeaveModel model = new MyLeaveModel();

                                model.setUserId(innerObj.getString("user_id"));
                                model.setUserName(innerObj.getString("user_name"));
                                model.setLeaveId(innerObj.getString("leave_id"));
                                model.setStartDate(innerObj.getString("start_date"));
                                model.setEndDate(innerObj.getString("end_date"));
                                model.setHours(innerObj.getString("hours"));
                                model.setReasonToLeave(innerObj.getString("reason_to_leave"));
                                model.setReasonToApprove(innerObj.getString("reason_to_approve"));
                                model.setLeaveCreateTime(innerObj.getString("leave_create_time"));
                                model.setStatus(innerObj.getString("status"));

                                arrayList.add(model);
                            }

                            for(int i = 0 ; i < arrayList.size(); i++){
                                List<Date> dates = getDates(arrayList.get(i).getStartDate(),arrayList.get(i).getEndDate());
                                totalLeave += dates.size();
                                for(Date date:dates){
                                    SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                                    Date dt = null;
                                    try {
                                        dt = sdf.parse(date.toString());
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    long millis = dt.getTime();

                                    List<Event> events = getEvents(millis,arrayList.get(i));

                                    ccvMyLeave.addEvents(events);
                                }
                            }
                        } else {
                            //Helper.showToast(getActivity(), message);
                        }

                        tvTotalLeave.setText("Total Leave : "+totalLeave);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_id);
            params.put("month", month);

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_MY_LEAVE, params, true);
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }

    private List<Event> getEvents(long timeInMillis, MyLeaveModel model) {
        return Arrays.asList(new Event(ContextCompat.getColor(getActivity(),R.color.colorPrimary), timeInMillis, model));
    }

    private ArrayList<Date> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }

        //EEE MMM dd HH:mm:ss zzz yyyy
        /*Fri Jun 01 00:00:00 GMT+05:30 2018
        Sat Jun 02 00:00:00 GMT+05:30 2018*/

        return dates;
    }
}
