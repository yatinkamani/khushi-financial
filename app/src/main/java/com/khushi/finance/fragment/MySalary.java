package com.khushi.finance.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khushi.finance.R;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.adapter.SalaryAdapter;
import com.khushi.finance.model.SalaryModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MySalary extends Fragment {

    private ConstraintLayout clNoData;
    private RecyclerView rvSalary;
    private ArrayList<SalaryModel> arrayList;
    private SalaryAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_salary, container, false);
        ((MainActivity) getActivity()).disableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("My Salary");

        clNoData = (ConstraintLayout) view.findViewById(R.id.clNoData);

        rvSalary = (RecyclerView) view.findViewById(R.id.rvSalary);
        rvSalary.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSalary.setHasFixedSize(true);

        getMySalary();

        return view;
    }

    private void getMySalary() {

        arrayList = new ArrayList<>();

        SharedPreferences preferences = getActivity().getSharedPreferences(Services.PREF_NAME, Context.MODE_PRIVATE);
        String user_id = preferences.getString("user_id", "");

        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get Salary : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                SalaryModel model = new SalaryModel();

                                //model.setQuery_id(innerObj.getString("query_id"));

                                arrayList.add(model);
                            }
                            if (arrayList.size() > 0) {
                                clNoData.setVisibility(View.GONE);
                            }
                        } else {
                            clNoData.setVisibility(View.VISIBLE);
                        }
                        adapter = new SalaryAdapter(getActivity(), arrayList);
                        rvSalary.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_id);

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_SALARY, params, true);
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }
}
