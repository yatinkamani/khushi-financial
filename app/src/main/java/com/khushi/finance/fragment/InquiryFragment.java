package com.khushi.finance.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khushi.finance.R;
import com.khushi.finance.activity.CreateInquiryActivity;
import com.khushi.finance.activity.InquiryDetailsActivity;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.adapter.InquiryAdapter;
import com.khushi.finance.model.InquiryModel;
import com.khushi.finance.model.ServicesModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.RecyclerItemClickListener;
import com.khushi.finance.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class InquiryFragment extends Fragment {

    private FloatingActionButton fabAddInquiry;
    private RecyclerView rv_query;
    private ConstraintLayout clNoData;
    private ArrayList<InquiryModel> arrayList;
    private InquiryAdapter adapter;
    private int RESULT_ADD_INQUIRY = 52;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inquiry, container, false);
        ((MainActivity) getActivity()).disableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("Inquiry");

        SharedPreferences preferences = getActivity().getSharedPreferences(Services.PREF_NAME, Context.MODE_PRIVATE);
        String designation = preferences.getString("designation", "");

        if (designation.toLowerCase().equalsIgnoreCase("salesman")) {
            ((MainActivity) getActivity()).toolbarTitle("DCR (Daily Call Record)");
        }

        clNoData = (ConstraintLayout) view.findViewById(R.id.clNoData);

        fabAddInquiry = (FloatingActionButton) view.findViewById(R.id.fabAddInquiry);
        fabAddInquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), CreateInquiryActivity.class), RESULT_ADD_INQUIRY);
            }
        });

        rv_query = (RecyclerView) view.findViewById(R.id.rv_query);
        rv_query.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_query.setHasFixedSize(true);

        getServices();

        rv_query.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), rv_query, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), InquiryDetailsActivity.class);
                intent.putExtra("inquiry", arrayList.get(position));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        return view;
    }

    private void getServices() {

        final ArrayList<ServicesModel> arrayListService = new ArrayList<>();

        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("GET Services : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {

                            JSONArray result = object.getJSONArray("data");
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject innerObject = result.getJSONObject(i);

                                ServicesModel model = new ServicesModel();
                                model.setServiceId(innerObject.getString("service_id"));
                                model.setServiceName(innerObject.getString("service_name"));
                                model.setSelected(false);

                                arrayListService.add(model);
                            }
                            new Helper().setServices(arrayListService);
                            getQuery();
                        } else {
                            Helper.showToast(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_SERVICE, new HashMap<String, String>(), true);
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }

    private void getQuery() {

        arrayList = new ArrayList<>();
        arrayList.clear();

        SharedPreferences preferences = getActivity().getSharedPreferences(Services.PREF_NAME, Context.MODE_PRIVATE);
        String user_id = preferences.getString("user_id", "");
        String login_status = preferences.getString("login_status", "");

        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get Inquiry : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                InquiryModel model = new InquiryModel();

                                model.setQuery_id(innerObj.getString("query_id"));
                                model.setName(innerObj.getString("name"));
                                model.setUsername(innerObj.getString("username"));
                                model.setEmail(innerObj.getString("email"));
                                model.setPhone(innerObj.getString("phone"));
                                model.setServices_name(innerObj.getString("services_name"));
                                model.setMessage(innerObj.getString("message"));
                                model.setImage1(innerObj.getString("image1"));
                                model.setImage2(innerObj.getString("image2"));
                                model.setImage3(innerObj.getString("image3"));
                                model.setImage4(innerObj.getString("image4"));
                                model.setImage5(innerObj.getString("image5"));

                                if(innerObj.getString("created_time").trim().length()>0){
                                    model.setCreatedTime(new Helper().formattedDateTime(innerObj.getString("created_time")));
                                } else {
                                    model.setCreatedTime(innerObj.getString("created_time").trim());
                                }

                                if(innerObj.getString("assigned_time").trim().length()>0){
                                    model.setAssignedTime(new Helper().formattedDateTime(innerObj.getString("assigned_time")));
                                } else {
                                    model.setAssignedTime(innerObj.getString("assigned_time").trim());
                                }

                                arrayList.add(model);
                            }

                            if(arrayList.size()>0){
                                clNoData.setVisibility(View.GONE);
                            }
                        } else {
                            clNoData.setVisibility(View.VISIBLE);
                        }

                        adapter = new InquiryAdapter(getActivity(), arrayList);
                        rv_query.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_id);
            params.put("login_status", login_status);

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_INQUIRY, params, true);
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_ADD_INQUIRY) {
            getQuery();
        }
    }
}
