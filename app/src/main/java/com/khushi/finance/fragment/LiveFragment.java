package com.khushi.finance.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.khushi.finance.R;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.adapter.LiveAdapter;
import com.khushi.finance.model.LiveModel;
import com.khushi.finance.utils.DividerItemDecoration;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.Services;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LiveFragment extends Fragment {

    private RecyclerView rvLive;
    private ArrayList<LiveModel> arrayList;
    private LiveAdapter liveAdapter;
    private ProgressDialog prd;
    private TextView tv_information,tv_symbol,tv_last_refreshed,tv_interval,tv_time_zone;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_live, container, false);
        ((MainActivity) getActivity()).disableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("Live");

        rvLive = (RecyclerView) view.findViewById(R.id.rvLive);
        rvLive.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvLive.setHasFixedSize(true);
        rvLive.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getActivity(), R.drawable.rv_divider)));

        tv_information = (TextView) view.findViewById(R.id.tv_information);
        tv_symbol = (TextView) view.findViewById(R.id.tv_symbol);
        tv_last_refreshed = (TextView) view.findViewById(R.id.tv_last_refreshed);
        tv_interval = (TextView) view.findViewById(R.id.tv_interval);
        tv_time_zone = (TextView) view.findViewById(R.id.tv_time_zone);

        getLive();

        return view;
    }

    private void getLive() {

        prd = new ProgressDialog(getActivity());
        prd.setMessage("Please, Wait a moment");
        prd.setTitle("Processing...");

        arrayList = new ArrayList<>();

        if (Helper.isNetworkAvailable(getActivity())) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, Services.GET_LIVE, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (prd != null && prd.isShowing())
                        prd.dismiss();

                    try {
                        Helper.showLog("Get Live : " + response);
                        JSONObject object = new JSONObject(response);
                        JSONObject metaData = object.getJSONObject("Meta Data");

                        tv_information.setText(metaData.getString("1. Information"));
                        tv_symbol.setText(metaData.getString("2. Symbol"));
                        tv_last_refreshed.setText(metaData.getString("3. Last Refreshed"));
                        tv_interval.setText(metaData.getString("4. Interval"));
                        tv_time_zone.setText(metaData.getString("6. Time Zone"));

                        JSONObject time = object.getJSONObject("Time Series (1min)");
                        Iterator<String> iterator = time.keys();
                        while (iterator.hasNext()) {
                            String date = iterator.next().toString();
                            JSONObject dateJson = time.getJSONObject(date);

                            String open = dateJson.getString("1. open");
                            String high = dateJson.getString("2. high");
                            String low = dateJson.getString("3. low");
                            String close = dateJson.getString("4. close");
                            String volume = dateJson.getString("5. volume");

                            LiveModel model = new LiveModel();

                            model.setTime(date);
                            model.setOpen(open);
                            model.setHigh(high);
                            model.setLow(low);
                            model.setClose(close);
                            model.setVolume(volume);

                            arrayList.add(model);
                        }

                        liveAdapter = new LiveAdapter(getActivity(),arrayList);
                        rvLive.setAdapter(liveAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                            if (prd != null && prd.isShowing())
                                prd.dismiss();

                            String message = null;
                            if (volleyError instanceof NetworkError) {
                                message = "Please check your network connection!";
                            } else if (volleyError instanceof ServerError) {
                                message = "Please try again after some time!";
                            } else if (volleyError instanceof AuthFailureError) {
                                message = "Unauthorized access";
                            } else if (volleyError instanceof ParseError) {
                                message = "Parsing error! Please try again after some time!!";
                            } else if (volleyError instanceof NoConnectionError) {
                                message = "Please check your connection!";
                            } else if (volleyError instanceof TimeoutError) {
                                message = "Oops. Connection Timeout!";
                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();

                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);
            prd.show();
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }
}
