package com.khushi.finance.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.khushi.finance.R;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.Services;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;

public class SettingFragment extends Fragment {

    private SwitchCompat switchNotification;
    private SharedPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        ((MainActivity) getActivity()).disableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("Setting");

        preferences = getActivity().getSharedPreferences(Services.PREF_NAME, MODE_PRIVATE);

        switchNotification = (SwitchCompat) view.findViewById(R.id.switchNotification);

        switch (preferences.getString("notification", "")) {
            case "on":
                switchNotification.setChecked(true);
                break;

            case "off":
                switchNotification.setChecked(false);
                break;
        }

        switchNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    notificationSetting("on");
                } else {
                    notificationSetting("off");
                }
            }
        });

        return view;
    }

    private void notificationSetting(String status) {

        String user_id = preferences.getString("user_id", "");
        String designation = preferences.getString("designation", "");

        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Notification Setting : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONObject data = object.getJSONObject("data");
                            String notification = data.getString("notification");

                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("notification", notification);
                            editor.apply();
                        } else {
                            Helper.showToast(getActivity(), message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();
            params.put("user_id", user_id);
            params.put("designation", designation);
            params.put("status", status);

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.NOTIFICATION_SETTING, params, true);
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }
}
