package com.khushi.finance.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.PermissionChecker;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.activity.EmployeeDetailsActivity;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.adapter.ManageEmployeeAdapter;
import com.khushi.finance.model.ManageEmployeeModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.RecyclerItemClickListener;
import com.khushi.finance.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ManageEmployeeFragment extends Fragment {

    private RecyclerView rvManageEmployee;
    private SearchView searchView;
    private ConstraintLayout clNoData;
    private ManageEmployeeAdapter adapter;
    private ArrayList<ManageEmployeeModel> arrayList, arrayListFinal;
    private int REQUEST_CALL_PHONE = 989;
    private String DIAL_NUMBER;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manage_employee, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).disableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("Manage Employee");

        clNoData = (ConstraintLayout) view.findViewById(R.id.clNoData);

        rvManageEmployee = (RecyclerView) view.findViewById(R.id.rvManageEmployee);
        rvManageEmployee.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvManageEmployee.setHasFixedSize(true);

        getEmployee();

        rvManageEmployee.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), rvManageEmployee, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                TextView tv_phone = (TextView) view.findViewById(R.id.tv_phone);
                LinearLayout llUser = (LinearLayout) view.findViewById(R.id.llUser);

                tv_phone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!TextUtils.isEmpty(arrayListFinal.get(position).getUserPhone())) {
                            openDialPad(arrayListFinal.get(position).getUserPhone());
                        }
                    }
                });

                llUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), EmployeeDetailsActivity.class);
                        intent.putExtra("employee", arrayListFinal.get(position));
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        return view;
    }

    private void getEmployee() {

        arrayList = new ArrayList<>();
        arrayListFinal = new ArrayList<>();

        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get Employee : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                ManageEmployeeModel model = new ManageEmployeeModel();

                                model.setUserId(innerObj.getString("user_id"));
                                model.setUserName(innerObj.getString("user_name"));
                                model.setUserFirstname(innerObj.getString("user_firstname"));
                                model.setUserLastname(innerObj.getString("user_lastname"));
                                model.setUserEmail(innerObj.getString("user_email"));
                                model.setPersonalEmail(innerObj.getString("personal_user_email"));
                                model.setUserPhone(innerObj.getString("user_phone"));
                                model.setUserDId(innerObj.getString("user_d_id"));
                                model.setUserStatus(innerObj.getString("user_status"));
                                model.setUserAddress(innerObj.getString("user_address"));
                                model.setUserBDate(innerObj.getString("user_b_date"));
                                model.setUserJoiningDate(innerObj.getString("user_joining_date"));
                                model.setUserPhoto(innerObj.getString("user_photo"));

                                arrayList.add(model);
                            }

                            arrayListFinal = arrayList;

                            adapter = new ManageEmployeeAdapter(getActivity(), arrayList);
                            rvManageEmployee.setAdapter(adapter);
                        } else {
                            clNoData.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_EMPLOYEE, params, true);
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_manage_employee, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, 10);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setMaxWidth(android.R.attr.width);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            public boolean onQueryTextChange(String newText) {
                arrayListFinal = new ArrayList<>();

                if (TextUtils.isEmpty(newText)) {
                    arrayListFinal.addAll(arrayList);
                } else {
                    for (ManageEmployeeModel item : arrayList) {
                        if (item.getUserFirstname().toLowerCase().contains(newText.toLowerCase())) {
                            arrayListFinal.add(item);
                        } else if (item.getUserLastname().toLowerCase().contains(newText.toLowerCase())) {
                            arrayListFinal.add(item);
                        }
                    }
                }

                adapter = new ManageEmployeeAdapter(getActivity(), arrayListFinal);
                rvManageEmployee.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void openDialPad(String mobile) {
        DIAL_NUMBER = mobile;
        try {
            int permissionCheck = PermissionChecker.checkCallingOrSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                openDialer(mobile);
            } else {
                requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_CALL_PHONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void openDialer(String mobile) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + mobile));
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Helper.showToast(getActivity(),"No app found to perform this action");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CALL_PHONE) {
            int permissionCheck = PermissionChecker.checkCallingOrSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                openDialer(DIAL_NUMBER);
            }
        }
    }
}
