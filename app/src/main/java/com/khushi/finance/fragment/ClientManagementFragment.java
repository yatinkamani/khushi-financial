package com.khushi.finance.fragment;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.PermissionChecker;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.activity.ClientDetailsActivity;
import com.khushi.finance.activity.MainActivity;
import com.khushi.finance.adapter.ClientManagementAdapter;
import com.khushi.finance.model.ChildModel;
import com.khushi.finance.model.ClientManagementModel;
import com.khushi.finance.model.FamilyModel;
import com.khushi.finance.utils.API;
import com.khushi.finance.utils.APIResponse;
import com.khushi.finance.utils.Helper;
import com.khushi.finance.utils.RecyclerItemClickListener;
import com.khushi.finance.utils.Services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ClientManagementFragment extends Fragment {

    private RecyclerView rvClientManagement;
    private ClientManagementAdapter adapter;
    private ArrayList<ClientManagementModel> arrayList, arrayListFinal, arrayListIndividual, arrayListNonIndividual;
    private SearchView searchView;
    private int checkedItem = -1;
    private String[] filterTypes;
    private ConstraintLayout clNoData;
    private int REQUEST_CALL_PHONE = 989;
    private String DIAL_NUMBER;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_management, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).disableCollapse();
        ((MainActivity) getActivity()).toolbarTitle("Client Management");

        clNoData = (ConstraintLayout) view.findViewById(R.id.clNoData);

        rvClientManagement = (RecyclerView) view.findViewById(R.id.rvClientManagement);
        rvClientManagement.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvClientManagement.setHasFixedSize(true);

        filterTypes = getResources().getStringArray(R.array.clinet_management_filter);

        getClient();

        rvClientManagement.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), rvClientManagement, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, final int position) {
                TextView tv_mobile = (TextView) view.findViewById(R.id.tv_mobile);
                TextView tv_phone = (TextView) view.findViewById(R.id.tv_phone);
                LinearLayout llUser = (LinearLayout) view.findViewById(R.id.llUser);

                tv_mobile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!TextUtils.isEmpty(arrayListFinal.get(position).getMobile())) {
                            openDialPad(arrayListFinal.get(position).getMobile());
                        }
                    }
                });

                tv_phone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!TextUtils.isEmpty(arrayListFinal.get(position).getPhoneNo())) {
                            openDialPad(arrayListFinal.get(position).getPhoneNo());
                        }
                    }
                });

                llUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), ClientDetailsActivity.class);
                        intent.putExtra("client", arrayListFinal.get(position));
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

        return view;
    }

    private void openDialPad(String mobile) {
        DIAL_NUMBER = mobile;
        try {
            int permissionCheck = PermissionChecker.checkCallingOrSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                openDialer(mobile);
            } else {
                requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_CALL_PHONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void openDialer(String mobile) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + mobile));
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Helper.showToast(getActivity(), "No app found to perform this action");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CALL_PHONE) {
            int permissionCheck = PermissionChecker.checkCallingOrSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                openDialer(DIAL_NUMBER);
            }
        }
    }

    private void getClient() {
        arrayList = new ArrayList<>();
        arrayListFinal = new ArrayList<>();
        arrayListIndividual = new ArrayList<>();
        arrayListNonIndividual = new ArrayList<>();

        if (Helper.isNetworkAvailable(getActivity())) {
            APIResponse apiResponse = new APIResponse() {
                @Override
                public void onAPISuccess(int requestCode, boolean isSuccess, String response) {
                    try {
                        Helper.showLog("Get Client : " + response);
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        String message = object.getString("message");
                        if (status.equalsIgnoreCase("true")) {
                            JSONArray data = object.getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject innerObj = data.getJSONObject(i);

                                ClientManagementModel model = new ClientManagementModel();

                                model.setClientId(innerObj.getString("client_id"));
                                model.setClientType(innerObj.getString("client_type"));
                                model.setClientInitial(innerObj.getString("client_initial"));
                                model.setFirstName(innerObj.getString("first_name"));
                                model.setMiddleName(innerObj.getString("middle_name"));
                                model.setLastName(innerObj.getString("last_name"));
                                model.setClientDob(innerObj.getString("client_dob"));
                                model.setGender(innerObj.getString("gender"));
                                model.setClientEducation(innerObj.getString("client_education"));
                                model.setNationality(innerObj.getString("nationality"));
                                model.setFreeHealthCheckup(innerObj.getString("free_helth_checkup"));
                                model.setHealthCheckupYear(innerObj.getString("health_checkup_year"));
                                model.setMutualFund(innerObj.getString("mutual_fund"));
                                model.setInsurance(innerObj.getString("insurance"));
                                model.setLoan(innerObj.getString("loan"));
                                model.setResidentialStatus(innerObj.getString("residential_status"));
                                model.setMeritalStatus(innerObj.getString("merital_status"));
                                model.setAnniversaryDate(innerObj.getString("anniversary_date"));
                                model.setPanNumber(innerObj.getString("pan_number"));
                                model.setKycProcessed(innerObj.getString("kyc_processed"));
                                model.setOccupation(innerObj.getString("occupation"));
                                model.setOccupationOther(innerObj.getString("occupation_other"));
                                model.setMobile(innerObj.getString("mobile"));
                                model.setPhoneNo(innerObj.getString("phone_no"));
                                model.setEmail(innerObj.getString("email"));
                                model.setLoginUsername(innerObj.getString("login_username"));
                                model.setTaxStatus(innerObj.getString("tax_status"));
                                model.setPlaceOfIncorporation(innerObj.getString("place_of_incorporation"));
                                model.setIndustry(innerObj.getString("industry"));
                                model.setDateOfIncorporation(innerObj.getString("date_of_incorporation"));
                                model.setDesignation(innerObj.getString("designation"));
                                model.setCompanyWebsite(innerObj.getString("company_website"));
                                model.setAddress(innerObj.getString("address"));
                                model.setCountry(innerObj.getString("country"));
                                model.setState(innerObj.getString("state"));
                                model.setCity(innerObj.getString("city"));
                                model.setPincode(innerObj.getString("pincode"));
                                model.setAmount(innerObj.getString("amount"));
                                model.setChequeNumber(innerObj.getString("cheque_number"));
                                model.setChequeDate(innerObj.getString("cheque_date"));
                                model.setBankName(innerObj.getString("bank_name"));
                                model.setBankBranch(innerObj.getString("bank_branch"));
                                model.setUserPhoto(innerObj.getString("user_photo"));

                                /***************************** PROCESS FAMILY DATA ********************************/
                                ArrayList<FamilyModel> familyAryLst = new ArrayList<>();
                                JSONArray family = innerObj.getJSONArray("family");
                                if(family.length()>0){
                                    for (int k = 0; k < family.length(); k++) {

                                        JSONObject familyObj = family.getJSONObject(k);

                                        FamilyModel fm = new FamilyModel();
                                        fm.setFamilyId(familyObj.getString("family_id"));
                                        fm.setClientId(familyObj.getString("client_id"));
                                        fm.setFamilyName(familyObj.getString("family_name"));
                                        fm.setFamilyDOB(familyObj.getString("family_date_of_birth"));
                                        fm.setFamilyAge(familyObj.getString("age"));
                                        fm.setFamilyRelation(familyObj.getString("relation"));

                                        familyAryLst.add(fm);
                                    }
                                }
                                model.setFamilyModelArrayList(familyAryLst);
                                /*************************** END OF PROCESS FAMILY DATA ********************************/

                                /*************************** PROCESS CHILD DATA ********************************/
                                ArrayList<ChildModel> childAryLst = new ArrayList<>();
                                JSONArray child = innerObj.getJSONArray("child");
                                if (child.length() > 0) {
                                    for (int k = 0; k < child.length(); k++) {

                                        JSONObject clientObj = child.getJSONObject(k);

                                        ChildModel cm = new ChildModel();
                                        cm.setChild_id(clientObj.getString("child_id"));
                                        cm.setClient_id(clientObj.getString("client_id"));
                                        cm.setChild_name(clientObj.getString("child_name"));
                                        cm.setChild_age(clientObj.getString("child_age"));
                                        cm.setEducation_age(clientObj.getString("education_age"));
                                        cm.setEducation_total_cost(clientObj.getString("education_total_cost"));
                                        cm.setMarriage_age(clientObj.getString("marriage_age"));
                                        cm.setMarriage_total_cost(clientObj.getString("marriage_total_cost"));

                                        childAryLst.add(cm);
                                    }
                                }
                                model.setChildArrayList(childAryLst);
                                /*************************** END OF PROCESS CHILD DATA ********************************/

                                arrayList.add(model);
                                arrayListFinal = arrayList;

                                if (innerObj.getString("client_type").toLowerCase().equalsIgnoreCase("non-individual")) {
                                    arrayListNonIndividual.add(model);
                                } else if (innerObj.getString("client_type").toLowerCase().equalsIgnoreCase("individual")) {
                                    arrayListIndividual.add(model);
                                }
                            }
                            adapter = new ClientManagementAdapter(getActivity(), arrayListFinal);
                            rvClientManagement.setAdapter(adapter);
                        } else {
                            clNoData.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAPIError(int requestCode, boolean isError, String error) {
                    if (isError) {
                        Helper.showToast(getActivity(), error);
                    }
                }
            };

            HashMap<String, String> params = new HashMap<>();

            API api = new API(getActivity(), apiResponse);
            api.execute(1, Services.GET_CLIENT, params, true);
        } else {
            Helper.showToast(getActivity(), Services.NO_NETWORK);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_client_management, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, 10);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setMaxWidth(android.R.attr.width);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextSubmit(String query) {
                adapter.filter(query);
                return false;
            }

            public boolean onQueryTextChange(String newText) {
                //adapter.filter(newText);


                arrayListFinal = new ArrayList<>();

                if (TextUtils.isEmpty(newText)) {
                    arrayListFinal.addAll(arrayList);
                } else {
                    for (ClientManagementModel item : arrayList) {
                        if (item.getFirstName().toLowerCase().contains(newText.toLowerCase())) {
                            arrayListFinal.add(item);
                        } else if (item.getLastName().toLowerCase().contains(newText.toLowerCase())) {
                            arrayListFinal.add(item);
                        }
                    }
                }


                adapter = new ClientManagementAdapter(getActivity(), arrayListFinal);
                rvClientManagement.setAdapter(adapter);
                adapter.notifyDataSetChanged();


                return false;
                /*arrayListFinal = filter(arrayList, newText, "");
                if (arrayListFinal != null || arrayListFinal.size() > 0) {
                    adapter.setFilter(arrayListFinal);
                } else {
                }

                return false;*/
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                classFilter();
                break;
        }
        return true;
    }

    public void classFilter() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle((CharSequence) "Select Filter");
        builder.setSingleChoiceItems(filterTypes, checkedItem, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                checkedItem = item;
            }
        });
        builder.setPositiveButton((CharSequence) "Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (checkedItem == 0) {
                    arrayListFinal = arrayList;
                    adapter = new ClientManagementAdapter(getActivity(), arrayListFinal);
                    rvClientManagement.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else if (checkedItem == 1) {
                    arrayListFinal = arrayListIndividual;
                    adapter = new ClientManagementAdapter(getActivity(), arrayListFinal);
                    rvClientManagement.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else if (checkedItem == 2) {
                    arrayListFinal = arrayListNonIndividual;
                    adapter = new ClientManagementAdapter(getActivity(), arrayListFinal);
                    rvClientManagement.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private ArrayList<ClientManagementModel> filter(ArrayList<ClientManagementModel> models, String query, String filterType) {
        query = query.toLowerCase();
        String type = filterType;
        ArrayList<ClientManagementModel> filteredModelList = new ArrayList();
        for (ClientManagementModel model : models) {
            String firstName = model.getFirstName().toString().toLowerCase().trim();
            String lastName = model.getLastName().toString().toLowerCase().trim();
            if (query.equals("")) {
                filteredModelList.add(model);
            } else if (firstName.contains(query)) {
                filteredModelList.add(model);
            } else if (lastName.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
}
