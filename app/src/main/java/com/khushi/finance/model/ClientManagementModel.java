package com.khushi.finance.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ClientManagementModel implements Serializable {
    String clientId, clientType, clientInitial, firstName, middleName, lastName, clientDob, gender, clientEducation, nationality, residentialStatus, meritalStatus, anniversaryDate, panNumber, kycProcessed, occupation, occupationOther, mobile, phoneNo, email, loginUsername, taxStatus, placeOfIncorporation, industry, dateOfIncorporation, designation, companyWebsite, address, country, state, city, pincode, amount, chequeNumber, chequeDate, bankName, bankBranch, userPhoto;
    ArrayList<ChildModel> childArrayList;
    ArrayList<FamilyModel> familyModelArrayList;
    String freeHealthCheckup, healthCheckupYear, mutualFund, insurance, loan;

    public String getFreeHealthCheckup() {
        return freeHealthCheckup;
    }

    public void setFreeHealthCheckup(String freeHealthCheckup) {
        this.freeHealthCheckup = freeHealthCheckup;
    }

    public String getHealthCheckupYear() {
        return healthCheckupYear;
    }

    public void setHealthCheckupYear(String healthCheckupYear) {
        this.healthCheckupYear = healthCheckupYear;
    }

    public String getMutualFund() {
        return mutualFund;
    }

    public void setMutualFund(String mutualFund) {
        this.mutualFund = mutualFund;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getLoan() {
        return loan;
    }

    public void setLoan(String loan) {
        this.loan = loan;
    }

    public ArrayList<ChildModel> getChildArrayList() {
        return childArrayList;
    }

    public void setChildArrayList(ArrayList<ChildModel> childArrayList) {
        this.childArrayList = childArrayList;
    }

    public ArrayList<FamilyModel> getFamilyModelArrayList() {
        return familyModelArrayList;
    }

    public void setFamilyModelArrayList(ArrayList<FamilyModel> familyModelArrayList) {
        this.familyModelArrayList = familyModelArrayList;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getClientInitial() {
        return clientInitial;
    }

    public void setClientInitial(String clientInitial) {
        this.clientInitial = clientInitial;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getClientDob() {
        return clientDob;
    }

    public void setClientDob(String clientDob) {
        this.clientDob = clientDob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClientEducation() {
        return clientEducation;
    }

    public void setClientEducation(String clientEducation) {
        this.clientEducation = clientEducation;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getResidentialStatus() {
        return residentialStatus;
    }

    public void setResidentialStatus(String residentialStatus) {
        this.residentialStatus = residentialStatus;
    }

    public String getMeritalStatus() {
        return meritalStatus;
    }

    public void setMeritalStatus(String meritalStatus) {
        this.meritalStatus = meritalStatus;
    }

    public String getAnniversaryDate() {
        return anniversaryDate;
    }

    public void setAnniversaryDate(String anniversaryDate) {
        this.anniversaryDate = anniversaryDate;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getKycProcessed() {
        return kycProcessed;
    }

    public void setKycProcessed(String kycProcessed) {
        this.kycProcessed = kycProcessed;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getOccupationOther() {
        return occupationOther;
    }

    public void setOccupationOther(String occupationOther) {
        this.occupationOther = occupationOther;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLoginUsername() {
        return loginUsername;
    }

    public void setLoginUsername(String loginUsername) {
        this.loginUsername = loginUsername;
    }

    public String getTaxStatus() {
        return taxStatus;
    }

    public void setTaxStatus(String taxStatus) {
        this.taxStatus = taxStatus;
    }

    public String getPlaceOfIncorporation() {
        return placeOfIncorporation;
    }

    public void setPlaceOfIncorporation(String placeOfIncorporation) {
        this.placeOfIncorporation = placeOfIncorporation;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getDateOfIncorporation() {
        return dateOfIncorporation;
    }

    public void setDateOfIncorporation(String dateOfIncorporation) {
        this.dateOfIncorporation = dateOfIncorporation;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCompanyWebsite() {
        return companyWebsite;
    }

    public void setCompanyWebsite(String companyWebsite) {
        this.companyWebsite = companyWebsite;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public String getChequeDate() {
        return chequeDate;
    }

    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }
}
