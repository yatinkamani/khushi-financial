package com.khushi.finance.model;

public class MenuModel {
    int iconSmall;
    int iconLarge;
    String menuTitle;

    public MenuModel(String menuTitle, int iconSmall, int iconLarge) {
        this.menuTitle = menuTitle;
        this.iconSmall = iconSmall;
        this.iconLarge = iconLarge;
    }

    public int getIconSmall() {
        return iconSmall;
    }

    public void setIconSmall(int iconSmall) {
        this.iconSmall = iconSmall;
    }

    public int getIconLarge() {
        return iconLarge;
    }

    public void setIconLarge(int iconLarge) {
        this.iconLarge = iconLarge;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }
}
