package com.khushi.finance.model;

import java.io.Serializable;

public class ChildModel implements Serializable{
    String child_id;
    String client_id;
    String child_name;
    String child_age;
    String education_age;
    String education_total_cost;
    String marriage_age;
    String marriage_total_cost;

    public String getChild_id() {
        return child_id;
    }

    public void setChild_id(String child_id) {
        this.child_id = child_id;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getChild_name() {
        return child_name;
    }

    public void setChild_name(String child_name) {
        this.child_name = child_name;
    }

    public String getChild_age() {
        return child_age;
    }

    public void setChild_age(String child_age) {
        this.child_age = child_age;
    }

    public String getEducation_age() {
        return education_age;
    }

    public void setEducation_age(String education_age) {
        this.education_age = education_age;
    }

    public String getEducation_total_cost() {
        return education_total_cost;
    }

    public void setEducation_total_cost(String education_total_cost) {
        this.education_total_cost = education_total_cost;
    }

    public String getMarriage_age() {
        return marriage_age;
    }

    public void setMarriage_age(String marriage_age) {
        this.marriage_age = marriage_age;
    }

    public String getMarriage_total_cost() {
        return marriage_total_cost;
    }

    public void setMarriage_total_cost(String marriage_total_cost) {
        this.marriage_total_cost = marriage_total_cost;
    }
}
