package com.khushi.finance.model;

public class DocumentModel {
    String document_id, user_document_image;

    public String getDocument_id() {
        return document_id;
    }

    public void setDocument_id(String document_id) {
        this.document_id = document_id;
    }

    public String getUser_document_image() {
        return user_document_image;
    }

    public void setUser_document_image(String user_document_image) {
        this.user_document_image = user_document_image;
    }
}
