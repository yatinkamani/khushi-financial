package com.khushi.finance.model;

public class MyLeaveModel {
    String userId;
    String userName;
    String leaveId;
    String startDate;
    String endDate;
    String reasonToLeave;
    String leaveCreateTime;
    String status;
    String reasonToApprove;
    String hours;

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(String leaveId) {
        this.leaveId = leaveId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getReasonToLeave() {
        return reasonToLeave;
    }

    public void setReasonToLeave(String reasonToLeave) {
        this.reasonToLeave = reasonToLeave;
    }

    public String getLeaveCreateTime() {
        return leaveCreateTime;
    }

    public void setLeaveCreateTime(String leaveCreateTime) {
        this.leaveCreateTime = leaveCreateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReasonToApprove() {
        return reasonToApprove;
    }

    public void setReasonToApprove(String reasonToApprove) {
        this.reasonToApprove = reasonToApprove;
    }
}
