package com.khushi.finance.model;

import java.io.Serializable;

/**
 * Created by Kaprat on 7/30/2018.
 */

public class PaymentDetailsModel implements Serializable {
    private String client_payment_id;

    public String getClient_payment_id() {
        return client_payment_id;
    }

    public void setClient_payment_id(String client_payment_id) {
        this.client_payment_id = client_payment_id;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getPayment_date() {
        return payment_date;
    }

    public void setPayment_date(String payment_date) {
        this.payment_date = payment_date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCheque_number() {
        return cheque_number;
    }

    public void setCheque_number(String cheque_number) {
        this.cheque_number = cheque_number;
    }

    public String getCheque_date() {
        return cheque_date;
    }

    public void setCheque_date(String cheque_date) {
        this.cheque_date = cheque_date;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_branch() {
        return bank_branch;
    }

    public void setBank_branch(String bank_branch) {
        this.bank_branch = bank_branch;
    }

    public String getClient_note() {
        return client_note;
    }

    public void setClient_note(String client_note) {
        this.client_note = client_note;
    }

    public String getPayment_create_time() {
        return payment_create_time;
    }

    public void setPayment_create_time(String payment_create_time) {
        this.payment_create_time = payment_create_time;
    }

    private String client_id;
    private String payment_date;
    private String amount;
    private String cheque_number;
    private String cheque_date;
    private String bank_name;
    private String bank_branch;
    private String client_note;
    private String payment_create_time;
}
