package com.khushi.finance.model;

public class LeaveHourModel {
    String leave;

    public LeaveHourModel(String leave) {
        this.leave = leave;
    }

    public String getLeave() {
        return leave;
    }

    public void setLeave(String leave) {
        this.leave = leave;
    }
}
