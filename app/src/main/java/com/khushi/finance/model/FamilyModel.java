package com.khushi.finance.model;

import java.io.Serializable;

public class FamilyModel implements Serializable {
    String clientId, familyId, familyName, familyDOB, familyAge, familyRelation;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getFamilyId() {
        return familyId;
    }

    public void setFamilyId(String familyId) {
        this.familyId = familyId;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getFamilyDOB() {
        return familyDOB;
    }

    public void setFamilyDOB(String familyDOB) {
        this.familyDOB = familyDOB;
    }

    public String getFamilyAge() {
        return familyAge;
    }

    public void setFamilyAge(String familyAge) {
        this.familyAge = familyAge;
    }

    public String getFamilyRelation() {
        return familyRelation;
    }

    public void setFamilyRelation(String familyRelation) {
        this.familyRelation = familyRelation;
    }
}
