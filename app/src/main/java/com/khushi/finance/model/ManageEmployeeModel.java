package com.khushi.finance.model;

import java.io.Serializable;

public class ManageEmployeeModel implements Serializable {
    String userId;
    String userName;
    String userFirstname;
    String userLastname;
    String userEmail;
    String userPhone;
    String userDId;
    String userStatus;
    String userAddress;
    String userBDate;
    String userJoiningDate;
    String userPhoto;

    public String getPersonalEmail() {
        return personalEmail;
    }

    public void setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
    }

    String personalEmail;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastname(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserDId() {
        return userDId;
    }

    public void setUserDId(String userDId) {
        this.userDId = userDId;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserBDate() {
        return userBDate;
    }

    public void setUserBDate(String userBDate) {
        this.userBDate = userBDate;
    }

    public String getUserJoiningDate() {
        return userJoiningDate;
    }

    public void setUserJoiningDate(String userJoiningDate) {
        this.userJoiningDate = userJoiningDate;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }
}
