package com.khushi.finance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.LiveModel;

import java.util.ArrayList;

public class LiveAdapter extends RecyclerView.Adapter<LiveAdapter.ViewHolder> {

    private Context context;
    private ArrayList<LiveModel> arrayList;

    public LiveAdapter(Context context, ArrayList<LiveModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public LiveAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_live, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LiveAdapter.ViewHolder holder, int position) {
        holder.tv_time.setText(arrayList.get(position).getTime());
        holder.tv_open.setText(arrayList.get(position).getOpen());
        holder.tv_high.setText(arrayList.get(position).getHigh());
        holder.tv_low.setText(arrayList.get(position).getLow());
        holder.tv_close.setText(arrayList.get(position).getClose());
        holder.tv_volume.setText(arrayList.get(position).getVolume());
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_time, tv_open, tv_high, tv_low, tv_close, tv_volume;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            tv_open = (TextView) itemView.findViewById(R.id.tv_open);
            tv_high = (TextView) itemView.findViewById(R.id.tv_high);
            tv_low = (TextView) itemView.findViewById(R.id.tv_low);
            tv_close = (TextView) itemView.findViewById(R.id.tv_close);
            tv_volume = (TextView) itemView.findViewById(R.id.tv_volume);
        }
    }
}
