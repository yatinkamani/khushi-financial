package com.khushi.finance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.FamilyModel;

import java.util.ArrayList;

public class FamilyAdapter extends RecyclerView.Adapter<FamilyAdapter.ViewHolder> {

    private Context context;
    private ArrayList<FamilyModel> arrayList;

    public FamilyAdapter(Context context, ArrayList<FamilyModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public FamilyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_family, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FamilyAdapter.ViewHolder holder, int position) {
        holder.tv_family_name.setText(arrayList.get(position).getFamilyName());
        holder.tv_family_dob.setText(arrayList.get(position).getFamilyDOB());
        holder.tv_family_age.setText(arrayList.get(position).getFamilyAge());
        holder.tv_family_relation.setText(arrayList.get(position).getFamilyRelation());
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_family_name, tv_family_dob, tv_family_age, tv_family_relation;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_family_name = (TextView) itemView.findViewById(R.id.tv_family_name);
            tv_family_dob = (TextView) itemView.findViewById(R.id.tv_family_dob);
            tv_family_age = (TextView) itemView.findViewById(R.id.tv_family_age);
            tv_family_relation = (TextView) itemView.findViewById(R.id.tv_family_relation);
        }
    }
}
