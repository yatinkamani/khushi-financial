package com.khushi.finance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.ManageEmployeeModel;
import com.khushi.finance.view.CircleImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ManageEmployeeAdapter extends RecyclerView.Adapter<ManageEmployeeAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ManageEmployeeModel> arrayList;

    public ManageEmployeeAdapter(Context context, ArrayList<ManageEmployeeModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ManageEmployeeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_management, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ManageEmployeeAdapter.ViewHolder holder, int position) {
        ManageEmployeeModel model = arrayList.get(position);

        holder.tv_user_name.setText(model.getUserFirstname() + " " + model.getUserLastname());
        holder.tv_phone.setText(model.getUserPhone());
        holder.llMobile.setVisibility(View.GONE);

        Picasso.get()
                .load(model.getUserPhoto())
                .resize(250, 250)
                .centerCrop()
                .error(R.drawable.image_not_available)
                .into(holder.iv_user_image);
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView iv_user_image;
        private TextView tv_user_name, tv_mobile,tv_phone;;
        private LinearLayout llUser,llMobile;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_user_image = (CircleImageView) itemView.findViewById(R.id.iv_user_image);
            tv_user_name = (TextView) itemView.findViewById(R.id.tv_user_name);
            tv_mobile = (TextView) itemView.findViewById(R.id.tv_mobile);
            tv_phone = (TextView) itemView.findViewById(R.id.tv_phone);
            llUser = (LinearLayout) itemView.findViewById(R.id.llUser);
            llMobile = (LinearLayout) itemView.findViewById(R.id.llMobile);

            itemView.setOnClickListener(null);
            tv_mobile.setOnClickListener(null);
            tv_phone.setOnClickListener(null);
            llUser.setOnClickListener(null);
        }
    }
}
