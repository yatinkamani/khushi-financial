package com.khushi.finance.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.khushi.finance.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class InquiryImageAdapter extends RecyclerView.Adapter<InquiryImageAdapter.ViewHolder> {

    private Context context;
    private ArrayList<String> arrayList;

    public InquiryImageAdapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_multiple_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.get()
                .load(arrayList.get(position))
                .resize(250,250)
                .centerCrop()
                .error(R.drawable.image_not_available)
                .into(holder.mi_Image);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mi_Image, mi_Close;

        public ViewHolder(View itemView) {
            super(itemView);

            mi_Image = (ImageView) itemView.findViewById(R.id.mi_Image);
            mi_Close = (ImageView) itemView.findViewById(R.id.mi_Close);

            mi_Close.setOnClickListener(null);
        }
    }
}
