package com.khushi.finance.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.esafirm.imagepicker.model.Image;
import com.khushi.finance.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class MultipleImageAdapter extends RecyclerView.Adapter<MultipleImageAdapter.ViewHolder> {

    private Context context;
    private List<Image> arrayList;

    public MultipleImageAdapter(Context context, List<Image> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_multiple_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String imagePath = arrayList.get(position).getPath();
        File file = new File(imagePath);

        if (file.exists()) {
            Bitmap selectedImage = null;
            try {
                selectedImage = decodeUri(Uri.fromFile(new File(imagePath)));
                //selectedImage = imageProcess(selectedImage);
                if (selectedImage != null) {
                    holder.mi_Image.setImageBitmap(selectedImage);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {
        // Decode image size 
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o);
        // The new size we want to scale to 
        final int REQUIRED_SIZE = 500;
        // Find the correct scale value. It should be the power of 2. 
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // Decode with inSampleSize 
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o2);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mi_Image, mi_Close;

        public ViewHolder(View itemView) {
            super(itemView);

            mi_Image = (ImageView) itemView.findViewById(R.id.mi_Image);
            mi_Close = (ImageView) itemView.findViewById(R.id.mi_Close);

            mi_Close.setOnClickListener(null);
        }
    }

//    private Bitmap imageProcess(Bitmap imageBitmap) {
//        int width = imageBitmap.getWidth();
//        int height = imageBitmap.getHeight();
//        int newWidth = 1000;
//        int newHeight = 1000;
//        if (width > 1000) {
//            double x = (double) width / 1000d;
//            newHeight = (int) (height / x);
//            newWidth = 1000;
//        } else if (height > 1000) {
//            double x = (double) height / 1000d;
//            newWidth = (int) (width / x);
//            newHeight = 1000;
//        }
//        // calculate the scale - in this case = 0.4f 
//        ExifInterface exif = null;
//        int rotationAngle = 0;
//        try {
//            exif = new ExifInterface(imageUri);
//            String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
//            int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
//            if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
//                rotationAngle = 90;
//            if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
//                rotationAngle = 180;
//            if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
//                rotationAngle = 270;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        float scaleWidth = ((float) newWidth) / width;
//        float scaleHeight = ((float) newHeight) / height;
//        Matrix matrix = new Matrix();
//        matrix.postScale(scaleWidth, scaleHeight);
//        matrix.postRotate(rotationAngle);
//        return Bitmap.createBitmap(imageBitmap, 0, 0, width, height, matrix, true);
//    }
}
