package com.khushi.finance.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.LeaveHourModel;

import java.util.ArrayList;

public class LeaveHourAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<LeaveHourModel> arrayList;
    private LayoutInflater inflter;

    public LeaveHourAdapter(Context context, ArrayList<LeaveHourModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.row_leave_hour, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        //names.setText(arrayList.get(i).getUserFirstname()+" "+arrayList.get(i).getUserLastname());
        names.setText(arrayList.get(i).getLeave());
        return view;
    }
}