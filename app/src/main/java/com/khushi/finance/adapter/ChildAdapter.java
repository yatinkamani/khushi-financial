package com.khushi.finance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.ChildModel;

import java.util.ArrayList;

public class ChildAdapter extends RecyclerView.Adapter<ChildAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ChildModel> arrayList;

    public ChildAdapter(Context context, ArrayList<ChildModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ChildAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_children, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildAdapter.ViewHolder holder, int position) {
        holder.tv_child_name.setText(arrayList.get(position).getChild_name());
        holder.tv_child_age.setText(arrayList.get(position).getChild_age());
        holder.tv_education_age.setText(arrayList.get(position).getEducation_age());
        holder.tv_education_total_cost.setText(arrayList.get(position).getEducation_total_cost());
        holder.tv_marriage_age.setText(arrayList.get(position).getMarriage_age());
        holder.tv_marriage_total_cost.setText(arrayList.get(position).getMarriage_total_cost());
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_child_name,tv_child_age,tv_education_age,tv_education_total_cost,tv_marriage_age,tv_marriage_total_cost;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_child_name = (TextView) itemView.findViewById(R.id.tv_child_name);
            tv_child_age = (TextView) itemView.findViewById(R.id.tv_child_age);
            tv_education_age = (TextView) itemView.findViewById(R.id.tv_education_age);
            tv_education_total_cost = (TextView) itemView.findViewById(R.id.tv_education_total_cost);
            tv_marriage_age = (TextView) itemView.findViewById(R.id.tv_marriage_age);
            tv_marriage_total_cost = (TextView) itemView.findViewById(R.id.tv_marriage_total_cost);
        }
    }
}
