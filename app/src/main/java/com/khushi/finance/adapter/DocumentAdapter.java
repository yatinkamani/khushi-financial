package com.khushi.finance.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.activity.ImageDetailsActivity;
import com.khushi.finance.model.DocumentModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.ViewHolder> {

    private Context context;
    private ArrayList<DocumentModel> arrayList;

    public DocumentAdapter(Context context, ArrayList<DocumentModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public DocumentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_docuemnt, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DocumentAdapter.ViewHolder holder, final int position) {
        Picasso.get()
                .load(arrayList.get(position).getUser_document_image())
                .resize(300,300)
                .centerCrop()
                .error(R.drawable.image_not_available)
                .into(holder.iv_document_image);

        holder.iv_document_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ImageDetailsActivity.class);
                intent.putExtra("image", arrayList.get(position).getUser_document_image());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_document_image;
        private TextView tv_document_name;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_document_image = (ImageView) itemView.findViewById(R.id.iv_document_image);
            tv_document_name = (TextView) itemView.findViewById(R.id.tv_document_name);
        }
    }
}
