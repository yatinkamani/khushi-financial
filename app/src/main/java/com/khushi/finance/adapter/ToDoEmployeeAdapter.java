package com.khushi.finance.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.ToDoEmployeeModel;

import java.util.ArrayList;

public class ToDoEmployeeAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ToDoEmployeeModel> arrayList;
    private LayoutInflater inflter;

    public ToDoEmployeeAdapter(Context context, ArrayList<ToDoEmployeeModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.row_to_do_employee, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        //names.setText(arrayList.get(i).getUserFirstname()+" "+arrayList.get(i).getUserLastname());
        names.setText(arrayList.get(i).getUserName());
        return view;
    }
}