package com.khushi.finance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.LeaveModel;

import java.util.ArrayList;

public class LeaveAdapter extends RecyclerView.Adapter<LeaveAdapter.ViewHolder> {

    private Context context;
    private ArrayList<LeaveModel> arrayList, filterList;

    public LeaveAdapter(Context context, ArrayList<LeaveModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        this.filterList = new ArrayList<LeaveModel>();
        this.filterList.addAll(this.arrayList);
    }

    @NonNull
    @Override
    public LeaveAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_leave, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LeaveAdapter.ViewHolder holder, int position) {
        LeaveModel model = arrayList.get(position);
        holder.tv_leave_name.setText(model.getUserName());
        holder.tv_leave_date.setText(model.getStartDate() + " - " + model.getEndDate());
        holder.tv_leave_status.setText(model.getStatus());

        switch (model.getStatus()) {
            case "Pending for Approval":
                holder.tv_leave_status.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;

            case "Approved":
                holder.tv_leave_status.setBackgroundColor(ContextCompat.getColor(context, R.color.approved));
                break;

            case "Declined":
                holder.tv_leave_status.setBackgroundColor(ContextCompat.getColor(context, R.color.declined));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return filterList == null ? 0 : filterList.size();
    }

    public void filter(final String text) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                filterList.clear();
                if (TextUtils.isEmpty(text)) {
                    filterList.addAll(arrayList);
                } else {
                    for (LeaveModel item : arrayList) {
                        if (item.getUserName().toLowerCase().contains(text.toLowerCase())) {
                            filterList.add(item);
                        }
                    }
                }

                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_leave_name, tv_leave_date, tv_leave_status;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_leave_name = (TextView) itemView.findViewById(R.id.tv_leave_name);
            tv_leave_date = (TextView) itemView.findViewById(R.id.tv_leave_date);
            tv_leave_status = (TextView) itemView.findViewById(R.id.tv_leave_status);
        }
    }
}
