package com.khushi.finance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.ToDoModel;

import java.util.ArrayList;

public class ToDoAdapter extends RecyclerView.Adapter<ToDoAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ToDoModel> arrayList;

    public ToDoAdapter(Context context, ArrayList<ToDoModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ToDoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_todo, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ToDoAdapter.ViewHolder holder, int position) {
        ToDoModel model = arrayList.get(position);

        String status = model.getTodoStatus();

        holder.tv_user_name.setText(model.getEmployeeUsername());
        holder.tv_todo_title.setText(model.getTodoMessage());
        holder.tv_todo_to.setText(model.getTodoTo());

        switch (status) {
            case "0":
                holder.tv_todo_status.setText("In-Complete");
                holder.llToDoStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                break;

            case "1":
                holder.tv_todo_status.setText("Completed");
                holder.llToDoStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.approved));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_user_name, tv_todo_title, tv_todo_status, tv_todo_to;
        private LinearLayout llToDoStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_user_name = (TextView) itemView.findViewById(R.id.tv_user_name);
            tv_todo_title = (TextView) itemView.findViewById(R.id.tv_todo_title);
            tv_todo_status = (TextView) itemView.findViewById(R.id.tv_todo_status);
            tv_todo_to = (TextView) itemView.findViewById(R.id.tv_todo_to);
            llToDoStatus = (LinearLayout) itemView.findViewById(R.id.llToDoStatus);
        }
    }
}
