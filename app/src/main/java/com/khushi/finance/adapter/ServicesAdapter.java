package com.khushi.finance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.ServicesModel;

import java.util.ArrayList;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ServicesModel> arrayList;

    public ServicesAdapter(Context context, ArrayList<ServicesModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_services,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicesAdapter.ViewHolder holder, int position) {
        holder.tv_service.setText(arrayList.get(position).getServiceName());
        if (arrayList.get(position).isSelected()) {
            holder.tv_selected.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        } else {
            holder.tv_selected.setBackgroundResource(R.drawable.service_check_box);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_selected,tv_service;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_selected = (TextView) itemView.findViewById(R.id.tv_selected);
            tv_service = (TextView) itemView.findViewById(R.id.tv_service);
        }
    }
}
