package com.khushi.finance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.PaymentDetailsModel;

import java.util.ArrayList;

/**
 * Created by Kaprat on 7/30/2018.
 */

public class PaymentDetalisAdapter extends RecyclerView.Adapter<PaymentDetalisAdapter.ViewHolder> {

    private Context context;
    private ArrayList<PaymentDetailsModel> arrayListPaymentDetails;

    public PaymentDetalisAdapter(Context context, ArrayList<PaymentDetailsModel> arrayListPaymentDetails){
        this.context = context;
        this.arrayListPaymentDetails = arrayListPaymentDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_payment_details, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tv_payment_date.setText(arrayListPaymentDetails.get(position).getPayment_date());
        holder.tv_amount.setText(arrayListPaymentDetails.get(position).getAmount());
        holder.tv_cheque_number.setText(arrayListPaymentDetails.get(position).getCheque_number());
        holder.tv_cheque_date.setText(arrayListPaymentDetails.get(position).getCheque_date());
        holder.tv_bank_name.setText(arrayListPaymentDetails.get(position).getBank_name());
        holder.tv_bank_branch.setText(arrayListPaymentDetails.get(position).getBank_branch());
        holder.tv_client_note.setText(arrayListPaymentDetails.get(position).getClient_note());
        holder.tv_payment_create_time.setText(arrayListPaymentDetails.get(position).getPayment_create_time());
    }

    @Override
    public int getItemCount() {
       return arrayListPaymentDetails == null ? 0 : arrayListPaymentDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_payment_date, tv_amount, tv_cheque_number, tv_cheque_date, tv_bank_name, tv_bank_branch,
                tv_client_note, tv_payment_create_time;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_payment_date = (TextView) itemView.findViewById(R.id.tv_payment_date);
            tv_amount = (TextView) itemView.findViewById(R.id.tv_amount);
            tv_cheque_number = (TextView) itemView.findViewById(R.id.tv_cheque_number);
            tv_cheque_date = (TextView) itemView.findViewById(R.id.tv_cheque_date);
            tv_bank_name = (TextView) itemView.findViewById(R.id.tv_bank_name);
            tv_bank_branch = (TextView) itemView.findViewById(R.id.tv_bank_branch);
            tv_client_note = (TextView) itemView.findViewById(R.id.tv_client_note);
            tv_payment_create_time = (TextView) itemView.findViewById(R.id.tv_payment_create_time);

        }
    }
}
