package com.khushi.finance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.ClientManagementModel;
import com.khushi.finance.view.CircleImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ClientManagementAdapter extends RecyclerView.Adapter<ClientManagementAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ClientManagementModel> arrayList, filterList;

    public ClientManagementAdapter(Context context, ArrayList<ClientManagementModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        this.filterList = new ArrayList<>();
        this.filterList.addAll(this.arrayList);
    }

    @NonNull
    @Override
    public ClientManagementAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_management, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientManagementAdapter.ViewHolder holder, int position) {
        ClientManagementModel model = arrayList.get(position);

        holder.tv_user_name.setText(model.getClientInitial() + " " + model.getFirstName() + " " + model.getLastName());
        holder.tv_mobile.setText(model.getMobile());
        holder.tv_phone.setText(model.getPhoneNo());

        Picasso.get()
                .load(model.getUserPhoto())
                .resize(250,250)
                .centerCrop()
                .error(R.drawable.image_not_available)
                .into(holder.iv_user_image);
    }

    @Override
    public int getItemCount() {
        return filterList == null ? 0 : filterList.size();
    }

    public void filter(final String text) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                filterList.clear();
                if (TextUtils.isEmpty(text)) {
                    filterList.addAll(arrayList);
                } else {
                    for (ClientManagementModel item : arrayList) {
                        if (item.getFirstName().toLowerCase().contains(text.toLowerCase())) {
                            filterList.add(item);
                        } else if(item.getLastName().toLowerCase().contains(text.toLowerCase())){
                            filterList.add(item);
                        }
                    }
                }

                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }

    public void setFilter(ArrayList<ClientManagementModel> list) {
        this.filterList = new ArrayList();
        this.filterList.addAll(list);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView iv_user_image;
        private TextView tv_user_name, tv_mobile,tv_phone;
        private LinearLayout llUser,llMobile;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_user_image = (CircleImageView) itemView.findViewById(R.id.iv_user_image);
            tv_user_name = (TextView) itemView.findViewById(R.id.tv_user_name);
            tv_mobile = (TextView) itemView.findViewById(R.id.tv_mobile);
            tv_phone = (TextView) itemView.findViewById(R.id.tv_phone);
            llMobile = (LinearLayout) itemView.findViewById(R.id.llMobile);
            llUser = (LinearLayout) itemView.findViewById(R.id.llUser);

            itemView.setOnClickListener(null);
            tv_mobile.setOnClickListener(null);
            tv_phone.setOnClickListener(null);
            llUser.setOnClickListener(null);
        }
    }
}
