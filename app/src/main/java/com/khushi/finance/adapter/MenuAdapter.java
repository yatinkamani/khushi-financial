package com.khushi.finance.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.khushi.finance.R;
import com.khushi.finance.model.MenuModel;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    private Context context;
    private ArrayList<MenuModel> arrayList;
    private int layoutManager;

    /**
     * @param context
     * @param arrayList
     * @param layoutManager LinearLayout : 1, GridLayout : 2
     */

    public MenuAdapter(Context context, ArrayList<MenuModel> arrayList, int layoutManager) {
        this.context = context;
        this.arrayList = arrayList;
        this.layoutManager = layoutManager;
    }

    @NonNull
    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        if(layoutManager==1){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_unregistered,parent,false);
        } else if(layoutManager==2){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dashboard_menu,parent,false);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.ViewHolder holder, int position) {
        if(layoutManager==1){
            holder.ivMenuIcon.setImageResource(arrayList.get(position).getIconSmall());
        } else if(layoutManager==2){
            holder.ivMenuIcon.setImageResource(arrayList.get(position).getIconLarge());
        }
        holder.tvMenuTitle.setText(arrayList.get(position).getMenuTitle());
    }

    @Override
    public int getItemCount() {
        return arrayList == null ? 0 : arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMenuTitle;
        private ImageView ivMenuIcon;
        public ViewHolder(View itemView) {
            super(itemView);
            tvMenuTitle = (TextView) itemView.findViewById(R.id.tvMenuTitle);
            ivMenuIcon = (ImageView) itemView.findViewById(R.id.ivMenuIcon);
        }
    }
}
